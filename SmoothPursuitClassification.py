#!/usr/bin/env python3
# -*- coding: utf8 -*-
import argparse, sys, logging, warnings
if sys.platform == 'win32':
    import winsound
from datetime import datetime
#py.init_notebook_mode(connected=True)
#pandas.set_option('display.precision', 6)
from pandas import DataFrame
from parsers.DataReader import DataReader
from parsers.DataExporter import DataExporter
from parsers.MultiData import MultiData
from algo.IVTFilter import IVTFilter
from algo.IBDT import IBDT, CLASSIFICATION, COORD_SYSTEM
from utils.SettingsReader import SettingsReader
from utils.Utils import STATUS_ENUM
from utils.EventUtils import EventUtils, OUTPUT_LAYOUT, OUTPUT_TYPE

#TODO --filter
#TODO расширение интервалов влево и вправо (для саккад - понятно, для фиксаций рисково, для pursuit - не понятно, там профиль скорости шумный)
class SmoothPursuitClassification:
    """Main class of the utility.
    Parses command line arguments and launches calculation.
    """
    def __init__(self):
        self.log_format= "%(levelname)s, %(asctime)s, file %(pathname)s, line %(lineno)s, %(message)s"
        logging.basicConfig(filename='debug.log',
                            level=logging.DEBUG,
                            format=self.log_format)
        self.logger = logging.getLogger()
        warnings.filterwarnings('ignore')
        self.PROJECT_NAME = 'Smooth Pursuit Classification'
        self.PROJECT_NAME_SHORT = 'SP-c'
        #self.SAMPLES_COMPONENTS_LIST = ['fixation', 'saccade', 'pursuit', 'smi-messages']  #NOTE deprecated
        self.SAMPLES_COMPONENTS_LIST = ['ibdt-events', 'ibdt-events-filtered', 'ibdt-events-bad', 'smi-messages']
        self.settings_reader = SettingsReader(self)
        self.data_reader = DataReader(self)
        self.data_exporter = DataExporter(self)
        self.multi_data = MultiData(self)
        str1 = datetime.now().strftime('%Y-%m-%d')
        str2 = '{0} script.'.format(self.PROJECT_NAME)
        print(str1)
        print(str2)
        self.console_contents = str1 + '\n'
        self.console_contents += str2 + '\n'

    #TODO total execution time
    #FIXME move to Utils
    def print_to_out(self, text:str, status:str = '') -> None:
        """Prints text to console.
        :param text: Text to print.
        :param status: status prefix to add, useful for warnings and successful operations.
        :return:
        """
        now = datetime.now().strftime('%H:%M:%S')
        if 'error' in text.lower() or 'unknown' in text.lower() or 'fail' in text.lower() or 'there is no' in text.lower() or status==STATUS_ENUM['ERROR']:
            if sys.platform == 'win32':
                winsound.Beep(120, 400)
        elif 'warn' in text.lower() or status==STATUS_ENUM['WARNING']:
            if sys.platform == 'win32':
                winsound.Beep(200, 150)
        elif 'success' in text.lower() or 'complete' in text.lower() or status==STATUS_ENUM['OK']:
            if sys.platform == 'win32':
                winsound.Beep(2000, 150)
        if status:
            status='[{0}] '.format(status.upper())
        text = f'{now} {status}{text}'
        self.console_contents += f'{text}\n'
        print(text)

    def print_error(self) -> None:
        """Prints current Exception info to console.
        :return: None.
        """
        eInfo = sys.exc_info()
        text = '{0}: {1}.'.format(eInfo[0].__name__, eInfo[1])
        self.console_contents += f'{text}\n'
        print(text)

    def save_console(self, save_dir:str) -> None:
        """Write current console contents to file.
        :param save_dir: Path to write into.
        :return:
        """
        report_file = open(save_dir + '/console.txt', 'w')
        report_file.write(self.console_contents)
        report_file.close()
        self.print_to_out('Console contents saved to file.')

#BUG trial interval tagging in messages does not always correspond to timestamps, because of precision errors??
#NOTE settings should at least contain samples fname
def main():
    parser = argparse.ArgumentParser(description='Launch SmoothPursuitClassification from the command line.')
    settings_file_group = parser.add_mutually_exclusive_group()
    settings_file_group.add_argument('-s', '--settings-file', type=str, help='Path to settings XML file.')
    job_group = parser.add_argument_group('job', 'Parameters of job running.')
    job_group.add_argument('--smooth', type=str, choices=['savgol', '1dspline', '2dspline', 'conv'], default='1dspline', help='Filter for gaze data smoothing.')
    job_group.add_argument('--algo', type=str, choices=['ibdt', 'ivvt', 'ivdt', 'ivt', 'idt'], default='ibdt', help='Algorithm name for classifying eye movements.')
    job_group.add_argument('--classifier', type=str, choices=['1dcnnbilstm', 'fasterrcnn', 'cnn', 'ssd', 'irf'], help='Neural network classifier to use.')
    job_group.add_argument('--backend', type=str, choices=['keras', 'tf', 'neon', 'sklearn'], default='keras', help='Deep learning library to use as a backend.')
    #job_group.add_argument('--util', type=str, choices=['event-props', 'event-filter'], default='event-props event-filter', nargs='*', help='Additional jobs to run.')
    args = parser.parse_args()
    spc = SmoothPursuitClassification()
    spc.print_to_out('Using CLI with args: {0}.'.format(args))
    if args.settings_file:
        spc.settings_reader.select(args.settings_file)
        spc.data_reader.read(spc.settings_reader, spc.multi_data)
        #----
        for (channel, id) in spc.multi_data.gen_channel_ids(channel='smi-samples'):
            #FIXME duplicate storage
            #NOTE smoothing is not useful for low-freq RED-m data; px/ms velocity suits better
            samples_data = spc.multi_data.get_channel_and_tag(channel, id, block='trial', ignore_empty=False)
            velocity_data = spc.multi_data.get_velocity(samples_data=samples_data, id=id, smooth=args.smooth, convert_to_deg=True)
            spc.multi_data.set_node(channel, id, velocity_data)
            spc.multi_data._attributes['output_layout'] = OUTPUT_LAYOUT['EVENT_INTERVALS']
            #algo DETECTORS
            #TODO R channel hard-coded - сделать через кастомный итератор внутри класса
            spc.print_to_out('Classifying.')
            if args.algo == 'ivt':
                algo = IVTFilter()
                algo.run_job(velocity_data[['Time', 'RVelocitySmoothed']], 150, 15, 0.250, 0.035)
                spc.print_to_out('I-VT filter finished, with parameters: {0}'.format(algo.print_params()))
                #FIXME abs ref
                #TODO ClassifierResultClass, +universal plots call
                spc.multi_data.set_node('fixation', id, algo.get_result_filtered(state='fixation'))
                spc.multi_data.set_node('saccade', id, algo.get_result_filtered(state='saccade'))
            elif args.algo == 'ibdt':
                #Validity columns irrelevant (RED-m); using pupil dia mm
                velocity_data['pupil_validity'] = 1
                if spc.multi_data._has_column('R Mapped Diameter [mm]', id):
                    bad_pupil_bool = (velocity_data['L Mapped Diameter [mm]'] == 0) & (velocity_data['R Mapped Diameter [mm]'] == 0)
                    velocity_data.loc[bad_pupil_bool, 'pupil_validity'] = 0
                columnsData = DataFrame({'Time':velocity_data['Time'] * 1000, 'confidence':velocity_data['pupil_validity'], 'x':velocity_data['R POR X [px]'], 'y':velocity_data['R POR Y [px]']})
                #columnsData = DataFrame({'Time':velocity_data['Time'] * 1000, 'confidence':confidence, 'x':velocity_data['RPORXDegSmoothed'], 'y':velocity_data['RPORYDegSmoothed']})
                algo = IBDT()
                #TODO пробовать подавать на вход куски записи разной длины
                #TODO пробовать записи из gc2014, а то саккады здесь странные
                evt_u = EventUtils(algo.run_job(columnsData, 80, 0.5, CLASSIFICATION['TERNARY'], coord_system=COORD_SYSTEM['CARTESIAN']), type=OUTPUT_TYPE['SPC_IBDT_RAW'])
                #filter.runJob(columnsData,  80, 0.5, CLASSIFICATION['TERNARY'], coord_system=COORD_SYSTEM['POLAR'])
                #FIXME assignment twice
                evt_fulldata = evt_u.generate_event_props(velocity_data, spc.multi_data._attributes['output_layout'])
                spc.multi_data.set_node('ibdt-events', id, evt_fulldata)
                evt_fulldata = spc.multi_data.get_channel_and_tag('ibdt-events', id, block='trial', ignore_empty=False)
                evt_fulldata = evt_u.apply_bad_event_filters(evt_fulldata)
                spc.multi_data.set_node('ibdt-events-filtered', id, evt_fulldata)
                # spc.multi_data.set_node('ibdt-events-bad', id, evt_baddata)
                spc.print_to_out('I-BDT classifier finished.')

            #neural network CLASSIFIERS
            #TODO converters and pipe, training / eval
            if args.classifier == '1dcnnbilstm':
                pass
                #nn.LSTM(spc, spc.multiData, settingsReader=spc.settingsReader)
            elif args.classifier == 'fasterrcnn':
                pass
                #nn.FasterRCNN()
        #EXPORTING all data
        messages_data = spc.multi_data.get_channel_and_tag('smi-messages', id, block='trial', ignore_empty=False)
        spc.multi_data.set_node('smi-messages', id, messages_data)
        spc.data_exporter.export_CSV(spc.multi_data)
        spc.print_to_out('Successful execution.', status=STATUS_ENUM['OK'])
        #input('Press Return to exit...')
        sys.exit()
    else:
        spc.print_to_out('Settings file was not specified. Unable to proceed.', status='error')
        input('Press Return to exit...')
        sys.exit()

if __name__ == "__main__":
    main()