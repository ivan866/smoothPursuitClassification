#TODO move whole class to IBDT.py
import numpy as np
IBDT_CLASS = {'FIXATION':0,
              'SACCADE':1,
              'PURSUIT':2,
              'NOISE':3,
              'UNDEF':4}
IBDT_CLASS_SHORT = ['fix','sac','pur','noi','undef']

class GazeDataEntry():
    """Helper class
    IS a data sample with only 4 values: timestamp, eyetracking quality confidence, X and Y coordinates.
    """
    def __init__(self, ts:float, confidence:float, x:float, y:float):
        #milliseconds!
        self.ts = ts
        self.confidence = confidence
        self.x = x
        self.y = y
        self.v = 0.0
        self.classification = IBDT_CLASS['UNDEF']

    def isFixation(self) -> bool:
        """
        :return:
        """
        return self.classification == IBDT_CLASS['FIXATION']

    def isSaccade(self) -> bool:
        """
        :return:
        """
        return self.classification == IBDT_CLASS['SACCADE']

    def isPursuit(self) -> bool:
        """
        :return:
        """
        return self.classification == IBDT_CLASS['PURSUIT']

    def isNoise(self) -> bool:
        """
        :return:
        """
        return self.classification == IBDT_CLASS['NOISE']

    def isUndef(self) -> bool:
        """
        :return:
        """
        return self.classification == IBDT_CLASS['UNDEF']

    def pause(self) -> None:
        """
        :return:
        """
        #return int(0)
        pass	#?