#!/usr/bin/env python3
# -*- coding: utf8 -*-
# Copyright (c) 2017, University of Tübingen
# 
# Permission to use, copy, modify, and distribute this software and its
# documentation for non-commercial purposes, without fee, and without a written
# agreement is hereby granted, provided that the above copyright notice and this
# paragraph and the following two paragraphs appear in all copies.
# 
# IN NO EVENT SHALL THE UNIVERSITY OF TÜBINGEN BE LIABLE TO ANY PARTY FOR DIRECT,
# INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS,
# ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF
# UNIVERSITY OF TÜBINGEN HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
# 
# UNIVERSITY OF TÜBINGEN SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND
# UNIVERSITY OF TÜBINGEN HAS NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT,
# UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
from collections import deque
import numpy as np
from pandas import DataFrame
import cv2
from algo.GazeData import GazeDataEntry, IBDT_CLASS
from utils import Utils
from utils.Utils import COORDINATES_MODE

CLASSIFICATION = {'TERNARY': 'ternary',
                  'BINARY': 'binary'}
COORD_SYSTEM = {'CARTESIAN':0,
                'POLAR':1}

#TODO еще раз все тестировать с новой пандой
#  1.0: nonzero -> to_numpy().nonzero(), -select, numpy.nan -> pandas.NA, <NA>, -df.ix
class IBDT():
    """Main class of the algorithm
    Contains methods for training and data manipulation.
    Confidence column is not meaningful for SMI eyetrackers. Replaced by 1-validity.
    Based on Santini, Fuhl, Kubler, Kasneci, University of Tubingen, 2016, 2017.
    Ported by ivan866 on 2018.12.19.
    """
    def __init__(self, max_saccade_duration_ms:float = 80, min_sample_confidence:float = 0.5, classification:str = CLASSIFICATION['TERNARY']):
        self.max_saccade_duration_ms = max_saccade_duration_ms
        self.min_sample_confidence = min_sample_confidence
        self.classification = classification

        #TODO maybe change to list
        self.window = deque()
        self.cur  = IBDT_Data(base = GazeDataEntry(ts=0.0, confidence=0.0, x=0.0, y=0.0))
        self.prev = IBDT_Data(base = GazeDataEntry(ts=0.0, confidence=0.0, x=0.0, y=0.0))
        self.first_point = False
        self.model = None
        self.fIdx = 0
        self.sIdx = 0
        self.fMean = None
        self.sMean = None

    #DATA calculating methods
    def add_point(self, entry:object) -> None:
        """Calculates velocity and classifies event type of the given sample, if already trained.
        Not a method for aggregating data samples. Method is for using the classifier.
        :param entry: IBDT_Data to classify event on.
        :return:
        """
        entry.base.classification = IBDT_CLASS['UNDEF']
        #Low confidence, ignore it
        if entry.base.confidence < self.min_sample_confidence:
            return None
        #Add new point to window and update previous valid point
        self.window.append( entry )
        self.prev = self.cur
        self.cur = self.window[len(self.window)-1]
        #Remove old entries from window
        while True:
            #TODO math.abs() needed??
            if (self.cur.base.ts - self.window[0].base.ts) > (2*self.max_saccade_duration_ms):
                self.window.popleft()
            else:
                break
        #First point being classified is a special case (since we classify interframe periods)
        if not self.prev:
            self.cur.base.classification = IBDT_CLASS['UNDEF']
            entry.base.classification = self.cur.base.classification
            return None
        #We have an intersample period, let's classify it
        self.cur.base.v = self._estimate_velocity(self.cur, self.prev)
        #Update the priors
        self._update_pursuit_prior()
        self.cur.fixation.prior = 1 - self.cur.pursuit.prior
        self.cur.saccade.prior = self.cur.fixation.prior
        #Update the likelihoods
        self._update_pursuit_likelihood()
        self._update_fixation_and_saccade_likelihood()
        #Update the posteriors
        self.cur.pursuit.update()
        self.cur.fixation.update()
        self.cur.saccade.update()
        #Decision
        if self.classification == CLASSIFICATION['TERNARY']:
            self._ternary_classification()
        elif self.classification == CLASSIFICATION['BINARY']:
            self._binary_classification()
        entry.classification = self.cur.base.classification

    def _train(self, gaze:list) -> None:
        """Perform training on the data specified, split velocity means on 2 clusters and update model hyperparameters.
        :param gaze: list of IBDT_Data to train on.
        :return:
        """
        samples = deque()
        #Find first valid sample
        for index in range(0, len(gaze)):
            previous = gaze[index]
            if (previous != gaze[len(gaze)-1]) and (previous.base.confidence < self.min_sample_confidence):
                previous.base.v = np.nan
                continue
            else:
                break
        #Estimate velocities for remaining training samples
        #TODO refactor to apply() on dataframe
        #TODO refactor to iterators.next() where possible
        for index2 in range(index+1, len(gaze)):
            g = gaze[index2]
            #not fully applicable to SMI data
            if g.base.confidence < self.min_sample_confidence:
                g.base.v = np.nan
                continue
            g.base.v = self._estimate_velocity(g, previous)
            if not np.isnan(g.base.v):
                samples.append(g.base.v)
            previous = g
        self.model = cv2.ml.EM_create()
        self.model.setClustersNumber(2)
        self.model.setCovarianceMatrixType(cv2.ml.EM_COV_MAT_GENERIC)
        #TODO + or binary OR??
        self.model.setTermCriteria((cv2.TERM_CRITERIA_COUNT + cv2.TERM_CRITERIA_EPS, 15000, 1e-6))
        self.model.trainEM(np.array(samples))

        self.fIdx = 0
        self.sIdx = 1
        means = self.model.getMeans()
        #higher mean velocities are saccades
        if means[0] > means[1]:
            self.fIdx = 1
            self.sIdx = 0
        self.fMean = means[self.fIdx]
        self.sMean = means[self.sIdx]

    #FIXME скорость должна считаться по GVEC[XYZ], ибо расстояние до экрана
    def _estimate_velocity(self, cur:object, prev:object) -> float:
        """Simple velocity, no smoothing, no angular speed.
        :param cur:
        :param prev:
        :return: velocity from Euclidean distance (incorrect)
        """
        if self.coord_system==COORD_SYSTEM['CARTESIAN']:
            dist = cv2.norm( np.array([cur.base.x, cur.base.y]), np.array([prev.base.x, prev.base.y]) )
        elif self.coord_system==COORD_SYSTEM['POLAR']:
            dist = Utils.get_separation(cur.base.x, cur.base.y, prev.base.x, prev.base.y, z=1, mode=COORDINATES_MODE['FROM_POLAR'])
        else:
            raise ValueError
        dt = cur.base.ts - prev.base.ts
        return dist / dt

    def run_job(self, data:DataFrame, max_saccade_duration_ms:float, min_sample_confidence:float, classification:str, coord_system:int) -> list:
        """Sets classifier parameters, runs classification and returns list of specified event type.
        :param data: pandas dataframe with 4 columns - time(ms), validity(higher is more valid), x(px or mm), y(px or mm).
        :param max_saccade_duration_ms: saccades longer than this will not be classified as saccades.
        :param min_sample_confidence: validity lower than this will be considered undefined sample.
        :param classification: int code whether to classify pursuits or fixations and saccades only.
        :param coord_system: cartesian or polar coordinates passed, affects velocity.
        :return: list of IBDT_Data with events classified.
        """
        self.max_saccade_duration_ms = max_saccade_duration_ms
        self.min_sample_confidence = min_sample_confidence
        self.classification = classification
        self.coord_system = coord_system
        #print('Using {} classification mode.'.format(classification))
        #----
        ibdtData = []
        for index, row in data.iterrows():
            ibdtData.append(IBDT_Data( base=GazeDataEntry(ts=row[0], confidence=row[1], x=row[2], y=row[3]) ))
        #----
        self._train(ibdtData)
        for pt in ibdtData:
            self.add_point(pt)
        self.result = ibdtData
        return self.result

    #UPDATE methods
    def _update_pursuit_prior(self) -> None:
        """Calculates mean of all ?but last window values.
        :return:
        """
        #TODO refactor whole method to list comprehension
        previous_likelihoods = []
        for index in range(0, len(self.window)-1):
            d = self.window[index]
            previous_likelihoods.append(d.pursuit.likelihood)
        self.cur.pursuit.prior = np.mean(previous_likelihoods)

    def _update_pursuit_likelihood(self) -> None:
        """Calculates proportion of all but first velocities which values fall between model hyperparameters.
        :return:
        """
        if len(self.window) < 2:
            return None
        movement = 0.0
        for index in range(1, len(self.window)):
            #if (d-v > 0) #original
            d = self.window[index]
            # adaptive: don't activate with too small or too large movements
            if (d.base.v > self.fMean) and (d.base.v < self.sMean):
                movement += 1
        n = len(self.window)-1
        movementRatio = float(movement / n)
        self.cur.pursuit.likelihood = movementRatio

    def _update_fixation_and_saccade_likelihood(self) -> None:
        """Queries the model for predicted likelihoods, writes them to cur sample.
        :return:
        """
        if self.cur.base.v < self.fMean:
            self.cur.fixation.likelihood = 1
            self.cur.saccade.likelihood = 0
            return None
        if self.cur.base.v > self.sMean:
            self.cur.fixation.likelihood = 0
            self.cur.saccade.likelihood = 1
            return None
        sample = np.array(self.cur.base.v)
        try:
            likelihoods = self.model.predict( sample )[1][0]
            self.cur.fixation.likelihood = likelihoods[self.fIdx]
            self.cur.saccade.likelihood = likelihoods[self.sIdx]
        #all zeros in sample
        except cv2.error:
            self.cur.fixation.likelihood = 0.0
            self.cur.saccade.likelihood = 0.0

    #STATUS methods
    def _binary_classification(self) -> None:
        """Simple 2-class likelihood comparison.
        :return:
        """
        print('binclass func')
        print(self.cur.fixation.likelihood, self.cur.saccade.likelihood)
        if self.cur.fixation.likelihood > self.cur.saccade.likelihood:
            self.cur.classification = IBDT_CLASS['FIXATION']
        else:
            self.cur.classification = IBDT_CLASS['SACCADE']

    def _ternary_classification(self) -> None:
        """3-class Bayesian posterior comparison.
        :return:
        """
        #Class that maximizes posterior probability
        maxPosterior = self.cur.fixation.posterior
        self.cur.base.classification = IBDT_CLASS['FIXATION']
        if self.cur.saccade.posterior > maxPosterior:
            self.cur.base.classification = IBDT_CLASS['SACCADE']
            maxPosterior = self.cur.saccade.posterior
        if self.cur.pursuit.posterior > maxPosterior:
            self.cur.base.classification = IBDT_CLASS['PURSUIT']
        #Catch up saccades as saccades
        if self.cur.base.v > self.sMean:
            self.cur.base.classification = IBDT_CLASS['SACCADE']

#----
class IBDT_Prob():
    """Helper class
    Contains attributes of a data sample.
    """
    def __init__(self, prior:float = 0.0, likelihood:float = 0.0, posterior:float = 0.0):
        self.prior = prior
        self.likelihood = likelihood
        self.posterior = posterior

    def update(self) -> None:
        """Posterior calculation.

        :param self:
        :return:
        """
        self.posterior = self.prior * self.likelihood

class IBDT_Data():
    """Helper class
    Makes a data sample.
    """
    #TODO adapt for direct dataframe input
    def __init__(self, base:GazeDataEntry):
        self.base = base

        self.pursuit = IBDT_Prob()
        self.fixation = IBDT_Prob()
        self.saccade = IBDT_Prob()