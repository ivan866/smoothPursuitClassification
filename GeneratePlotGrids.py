#!/usr/bin/env python3
# -*- coding: utf8 -*-
import os, sys, argparse, logging, gc, tracemalloc
from datetime import datetime
from utils import Utils
import numpy as np
from scipy.spatial import ConvexHull
from scipy.spatial.qhull import QhullError
import pandas as pd
from pandas import DataFrame
from parsers.DataReader import SMI_SAMPLE_HEADERS
from algo.GazeData import IBDT_CLASS
from utils.PlotUtils import *

#BUG тестить проблемы с памятью под Linux
VERSION = '0.1'
LOG_FNAME = 'plots_memory_leak.log'
LOG_FORMAT = '%(levelname)s, %(asctime)s, file %(pathname)s, line %(lineno)s, %(message)s'

def create_dir(pdir:str) -> str:
    now = Utils.get_now_string()
    save_dir = '{0}/PLOTS_{1}'.format(pdir, now)
    os.makedirs(save_dir)
    return save_dir

#tracemalloc.start()
parser = argparse.ArgumentParser(description='Plotting utility for "SPC", v.{}.'.format(VERSION))
parser.add_argument('--samples-file', type=str, help='Path to exported samples with velocity and trial tagged.')
parser.add_argument('--events-file', type=str, help='Path to events classified by "SPC" script.')
# IDEA по факту генерация графиков передается в R, можно использовать интерфейс пакета pandas2ri
#   уже не актуально 2020
parser.add_argument('--plots', type=str, choices=['samples', 'spatial-event', 'velocity', 'combi'], nargs='*', help='Kind of plots to generate.')
parser.add_argument('--gridw', type=int, default=5, help='How many plots on figure, horizontally (_w_idth).')
parser.add_argument('--gridh', type=int, default=4, help='Plot num, vertically (_h_eight).')
#NOTE tiff filesize 100 times larger!
parser.add_argument('--format', type=str, choices=['png', 'jpg', 'pdf', 'tiff'], default='pdf', help='Image format, either raster or vector.')
args = parser.parse_args()
plot_dir = create_dir(os.path.dirname(args.samples_file))
log_dir = '{0}/{1}'.format(plot_dir, LOG_FNAME)
#NOTE log size may exceed 30 MB with level=DEBUG
#logging.basicConfig(filename = log_dir, level = logging.DEBUG, format = LOG_FORMAT)
#logger = logging.getLogger()
#print('Logger initialised. See {}'.format(log_dir))
#FIXME headers
smp_taggeddata = pd.read_csv(args.samples_file, sep=',', error_bad_lines=True)
evt_taggeddata = pd.read_csv(args.events_file, sep=',', error_bad_lines=True)

def find_smp_range(range_start, range_end, smp_taggeddata:DataFrame) -> DataFrame:
    return smp_taggeddata.iloc[int(range_start):int(range_end)]

#FIXME plot_dir is str, passed by reference (??) multiple times
mltplot_fix = MultiPlot(args.gridh, args.gridw, name='{:s}-fix'.format(args.plots[0]), dir=plot_dir, format=args.format)
mltplot_sac = MultiPlot(args.gridh, args.gridw, name='{:s}-sac'.format(args.plots[0]), dir=plot_dir, format=args.format)
mltplot_pur = MultiPlot(args.gridh, args.gridw, name='{:s}-pur'.format(args.plots[0]), dir=plot_dir, format=args.format)
if 'velocity' in args.plots:
    mltplot_fixvel = MultiPlot(args.gridh, args.gridw, name='{:s}-fix'.format('velocity'), dir=plot_dir, format=args.format)
    mltplot_sacvel = MultiPlot(args.gridh, args.gridw, name='{:s}-sac'.format('velocity'), dir=plot_dir, format=args.format)
    mltplot_purvel = MultiPlot(args.gridh, args.gridw, name='{:s}-pur'.format('velocity'), dir=plot_dir, format=args.format)
for index, evt in evt_taggeddata.iterrows():
    smp = find_smp_range(evt.SampleIndexStart, evt.SampleIndexEnd + 1, smp_taggeddata)
    smp.prefix = find_smp_range(evt.SampleIndexStart - 7, evt.SampleIndexStart + 1, smp_taggeddata)
    smp.affix = find_smp_range(evt.SampleIndexEnd, evt.SampleIndexEnd + 8, smp_taggeddata)
    zero_time = smp.iloc[0]['Time']
    smp.insert(1, 'time_0b_ms', (smp['Time'] - zero_time) * 1000)
    smp.prefix.insert(1, 'time_0b_ms', (smp.prefix['Time'] - zero_time) * 1000)
    smp.affix.insert(1, 'time_0b_ms', (smp.affix['Time'] - zero_time) * 1000)
    try:
        hull = ConvexHull(np.array(smp[[SMI_SAMPLE_HEADERS['X'], SMI_SAMPLE_HEADERS['Y']]]))
    except (QhullError, AttributeError):
        hull = None
    if evt.class_id == IBDT_CLASS['FIXATION']:
        if 'spatial-event' in args.plots:
            mltplot_fix.spatial_fixation(smp, evt, hull=hull)
        if 'velocity' in args.plots:
            mltplot_fixvel.velocity_plot(smp, evt)
    elif evt.class_id == IBDT_CLASS['SACCADE'] or evt.class_id == IBDT_CLASS['PURSUIT']:
        if 'spatial-event' in args.plots:
            if evt.class_id == IBDT_CLASS['SACCADE']:
                mltplot_sac.spatial_saccade(smp, evt, hull=hull)
                if 'velocity' in args.plots:
                    mltplot_sacvel.velocity_plot(smp, evt)
            elif evt.class_id == IBDT_CLASS['PURSUIT']:
                mltplot_pur.spatial_saccade(smp, evt, hull=hull)
                if 'velocity' in args.plots:
                    mltplot_purvel.velocity_plot(smp, evt)
        elif 'combi' in args.plots:
            raise NotImplementedError
#2020.05.22
#FIXME workaround to force save even if figure quantity is too small
mltplot_fix.save_figures(auto = True)
mltplot_sac.save_figures(auto = True)
mltplot_pur.save_figures(auto = True)
if 'velocity' in args.plots:
    mltplot_fixvel.save_figures(auto = True)
    mltplot_sacvel.save_figures(auto = True)
    mltplot_purvel.save_figures(auto = True)

print('Plots generated in {}.'.format(plot_dir))
sys.exit()