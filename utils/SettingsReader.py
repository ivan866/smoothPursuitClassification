#!/usr/bin/env python3
# -*- coding: utf8 -*-
import os, hashlib, xml
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup
BeautifulSoup.DEFAULT_BUILDER_FEATURES = ['xml']
from tkinter import filedialog
import numpy as np
import pandas as pd
from utils import Utils
from utils.Utils import STATUS_ENUM

class SettingsReader:
    """Reads, parses and queries xml file with settings."""
    def __init__(self, main:object):
        self.main = main
        self.data_dir = None
        self.settings_file = None
        self.settings_tree = None
        self.settings = None

    def get_dir(self)->str:
        """Returns data directory containing this settings file.
        :return: file path str.
        """
        return self.data_dir

    def select(self, file:str=None) -> None:
        """Selects file with settings, specified literally by path string, opens dialog otherwise.
        :param file: Path string.
        :return: None
        """
        if not file:
            self.settings_file = filedialog.askopenfilename(filetypes = (("XML Markup", "*.xml"), ("All Files", "*.*")))
        else:
            self.settings_file = file
        if self.settings_file:
            self.data_dir = os.path.dirname(self.settings_file)
            self.main.print_to_out('Settings file selected.')
        else:
            self.main.print_to_out('WARNING: Nothing selected. Please retry.')

    def read(self) -> None:
        """Actually reads and parses xml file contents.
        :return: 
        """
        #self.main.logger.debug('reading settings...')
        try:
            self.settings_tree = ET.parse(self.settings_file)
            self.settings = self.settings_tree.getroot()
        except xml.etree.ElementTree.ParseError:
            self.main.print_error()
            self.main.print_to_out('ERROR: Bad settings file. Check your XML is valid.')
            return None
        except:
            self.main.print_error()
            self.main.print_to_out('ERROR: Parsing settings failed. Operation aborted.')
            return None
        self.main.print_to_out('Settings parsed (' + self.settings_file + ').', status=STATUS_ENUM['OK'])
        if len(self.get_intervals(block='interval', ignoreEmpty=True)) == 0:
            self.main.print_to_out('WARNING: No intervals specified in settings file. Only autotagged trials will be available.')

    #DATA FILTERING methods
    def gen_type_file(self, type:str) -> object:
        """Generator of ids of particular type present in settings.
        :param type:
        :return: File XML element from settings, if such file exists on disk.
        """
        found=False
        if self._check(full=True):
            for elem in self.get_types(type):
                file= '{0}/{1}'.format(self.data_dir, elem.get('path'))
                if os.path.exists(file):
                    if not found:
                        self.main.print_to_out('Reading {0} data...'.format(type))
                        found=True
                    #добавляем контрольную сумму в настройки
                    elem.set('md5', self._md5(file))
                    yield elem
                else:
                    self.main.print_to_out('WARNING: File specified in settings (' + os.path.basename(file) + ') does not exist!')

    def _subst_versatile_channels(self, channel: str) -> str:
        """Substitutes related versatile channel names, which should be the name of the source file type channel in settings.
        :param channel: versatile channel name, like samples or gaze.
        :return: master channel name.
        """
        #FIXME hotfix
        if channel in self.main.SAMPLES_COMPONENTS_LIST and self._has_type('smi-samples') and not self._has_type('saccade'):
            return 'smi-samples'
        else:
            return channel

    def get_ids(self, id:str) -> list:
        """Queries settings for nodes with particular id attribute.
        :param id: id string from settings.
        :return: A list of matches with this id.
        """
        return self.settings.findall("file[@id='"+id+"']")

    def get_types(self, type:str) -> list:
        """Returns all nodes from settings with this type attribute.
        :param type: type string from settings.
        :return: A list of file tags by type.
        """
        return self.settings.findall("file[@type='"+type+"']")

    def unique(self, element:str='file', field:str='') -> list:
        """Filters all specified elements by field and returns unique.
        :param element: On what element of settings to filter on.
        :param field: For what field to search for.
        :return: List of unique fields in these elements.
        """
        elements = self.settings.findall(element)
        l=[]
        for el in elements:
            l.append(el.get(field))
        return np.unique(l)

    def get_path_attr_by_id(self, type:str, id:str, absolute:bool = False) -> str:
        """Returns path of a file suitable as a record tag.
        Except it still contains type and id tags.
        :param type: type str from settings.
        :param id: id str from settings.
        :param absolute: whether to concatenate with a data_dir and leave extension.
        :return: path str.
        """
        type_zero_name = self._subst_versatile_channels(type)
        file = self.get_type_by_id(type_zero_name, id)
        path = file.get('path')
        if absolute:
            return '{0}/{1}'.format(self.data_dir, path)
        else:
            return os.path.splitext(path)[0]

    def get_type_by_id(self, type:str, id:str) -> object:
        """Filters settings nodes by both type and id.
        
        :param type: type string from settings.
        :param id: id string from settings.
        :return: ElementTree.Element or list of them.
        """
        #self.main.logger.debug('get type by id')
        return self.settings.find("file[@type='" + type + "'][@id='"+id+"']")

    def get_zero_time_by_id(self, type:str, id:str, parse:bool = True) -> object:
        """Resolves and returns zero_time attribute of a file tag.

        :param type: type string from settings.
        :param id: id string from settings.
        :param parse: bool whether to parse str to timedelta or not.
        :return: zero_time attribute in timedelta or str format, 0 or '0' if zero_time attribute not present.
        """
        file = self.get_type_by_id(type, id)
        zero_time = file.get('zero_time', default='0')
        if len(self.get_types(zero_time)):
            zero_time = self.get_type_by_id(zero_time, id).get('zero_time')
        # случай когда zero_time ссылается на другой тип, а он отсутствует
        try:
            Utils.parse_time(zero_time)
        except:
            self.main.report_error()
            self.main.set_status('ERROR: Probably zero_time attribute for type {0}, id {1} wrongly defined in settings.'.format(type, id))
            self.main.set_status('ERROR: Consider correcting it or omit file entirely.')
            raise

        if parse:
            return Utils.parse_time(zero_time)
        else:
            return zero_time

    #INTERVALS
    def get_interval_by_id(self, id:str, block:str) -> object:
        """Returns interval with particular id attribute.
        
        :param id: id string from interval.
        :param block: which type of block to query.
        :return: ElementTree.Element
        """
        return self.settings.find("{0}[@id='{1}']".format(block, id))

    def get_intervals(self, block:str, ignoreEmpty:bool=True) -> list:
        """Returns all intervals.
        
        :param block:
        :param ignoreEmpty: Whether to cut off the empty and utility intervals.
        :return: A list of interval nodes from settings.
        """
        #NOTE _ (underscore) intervals are considered special, but not empty!
        if ignoreEmpty:
            return [interval for interval in self.settings.findall(block) if interval.get('id')]
        else:
            return self.settings.findall(block)

    def get_start_time_by_id(self, id:str, block:str, format:bool=False) -> object:
        """Computes and returns start time of interval specified by its id.
        
        Based on start time and durations of previous intervals.
        
        :param id: id attribute of interval.
        :param block: block type (in settings).
        :param format: bool whether to convert time to str or not.
        :return: Start time of interval in timedelta object.
        """
        #self.main.logger.debug('get start time by id')
        ints = self.get_intervals(block=block, ignoreEmpty=False)
        start_time = Utils.parse_time(0)
        thisId = None
        for i in ints:
            thisId = i.get('id')
            if thisId == id:
                break
            duration = self.get_duration_by_id(thisId, block=block)
            start_time = start_time + duration

        if format:
            return str(start_time)
        else:
            return start_time

    def get_end_time_by_id(self, id:str, block:str, format:bool=False) -> object:
        """Computes and returns end time of interval specified by its id.
        
        Based on start time and duration of given interval.
        
        :param id: id attribute of interval.
        :param block:
        :param format: bool whether to convert time to str or not.
        :return: End time of interval in timedelta object.
        """
        end_time = self.get_start_time_by_id(id, block=block) + self.get_duration_by_id(id, block=block)
        if format:
            return str(end_time)
        else:
            return end_time


    def get_duration_by_id(self, id:str, block:str, parse:bool=True) -> object:
        """Returns duration of interval with this id.
        
        :param id: id attribute of interval.
        :param block:
        :param parse: bool whether to parse str to timedelta or not.
        :return: Duration of interval in timedelta or str format.
        """
        dur = self.get_interval_by_id(id, block=block).get('duration')
        if parse:
            return Utils.parse_time(dur)
        else:
            return dur

    def get_durations(self, block:str, parse:bool=True) -> list:
        """Returns a list of durations of all intervals.
        
        :param block: from which block to gather data.
        :param parse: bool whether to parse list items to timedelta or not.
        :return: A list.
        """
        durs = []
        for interval in self.get_intervals(block=block, ignoreEmpty=True):
            durs.append(self.get_duration_by_id(interval.get('id'), block=block, parse=parse))
        return durs

    def total_duration(self, block:str, parse:bool=True) -> object:
        """Returns total duration of all intervals.
        
        :param block:
        :param parse: bool whether to parse str to timedelta or not.
        :return: Duration of interval in timedelta or str format.
        """
        dur = pd.DataFrame(self.get_durations(block=block, parse=True)).sum()[0]
        if parse:
            return dur
        else:
            return dur.strftime('%M:%S.%f')



    def _has_type(self, type:str) -> bool:
        """Checks if such file type present.

        :param type:
        :return:
        """
        if type in self.unique(field='type'):
            return True
        else:
            return False



    def _check(self, full:bool=False) -> bool:
        """Returns True if settings are already selected, False otherwise.

        :param full: if to check settings actually read and parsed already.
        :return: A bool representing presence of settings file path.
        """
        #self.main.logger.debug('check settings')
        if not full:
            if self.settings_file:
                return True
            else:
                self.main.print_to_out('WARNING: Select settings first!')
                return False
        else:
            if self.settings:
                return True
            else:
                self.main.print_to_out('WARNING: Read and parse settings first!')
                return False

    def _report_contents(self):
        print(ET.tostring(self.settings))

    #TODO json output
    def save(self, save_dir:str) -> None:
        """Write current settings to file.
        
        :param save_dir: Path to write into.
        :return: 
        """
        bs = BeautifulSoup(ET.tostring(self.settings))
        f = open(save_dir + '/' + os.path.basename(self.settings_file), 'w')
        f.write(bs.prettify(formatter='minimal'))
        f.close()

    def _md5(self, fname:str) -> str:
        """Calculates and returns MD5 hash checksum of a file.

        From https://stackoverflow.com/a/3431838/2795533

        :param fname: file path.
        :return: md5 hex value.
        """
        hash_md5 = hashlib.md5()
        with open(fname, "rb") as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)
        return hash_md5.hexdigest()