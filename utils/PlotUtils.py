import os.path, gc
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
#from matplotlib.figure import Figure   #can it be used for inheritance?
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist, squareform
from scipy.spatial.qhull import QhullError
from parsers.DataReader import SMI_SAMPLE_HEADERS
from algo.GazeData import IBDT_CLASS, IBDT_CLASS_SHORT
from utils import Utils

SUBPLOT_SIZE = 2.5  #inches
AXES_LIMIT = 100    #сотня требует в районе 450 МБ

#TODO heatmap uses gaze 3d position and direction to project samples on sphere from inside; heatmap is spherical+able to render a plane projection, like an atlas
#TODO нужно создать график координации каналов
#TODO хотел сделать наследование от plt.Figure, но так не получается, потому что метод plt.figure() главнее
#  how to inherit Matplotlib classes
#SEE also Jupyter Notebook with Plotly / PLPlot / GnuPlot
#TODO MPEG output with matplotlib.animation.ffmpegbase (combi_plot zoomed, 16:9)
class MultiPlot():
    """Plotting methods with some useful data management methods.
    Class assumes a specific matplotlibrc file present, e.g. in working dir.
    Not supposed to show large interactive figures, just save a set of pdf files."""
    def __init__(self, axes_rows:int=10, axes_cols:int=10, suptitle:str= 'MULTIPLOT', metadata:dict=None, name = 'default', dir:str = 'PLOTS', format:str = 'png'):
        """Contains a multiple of Matplotlib figure objects and operates buffering.

        :param nrow: num of subplots
        :param ncol:
        :param suptitle: title of the figure containing many subplots
        :param metadata: dimensions of experiment
        :param name: any description of the plot group
        """
        gc.enable()
        self.figure_list = []
        self.suptitle = suptitle
        self.metadata = metadata
        self.name = name
        self.dir = dir
        self.format = format
        rowscols = axes_rows*axes_cols
        self.AXES_LIMIT = max(rowscols, int(AXES_LIMIT/rowscols)*rowscols)
        self.axes_rows = axes_rows
        self.axes_cols = axes_cols
        self.axes_total = 0  #считает сколько уже графиков нанесено
        self.axes_group = 0    #перед сливом группы
        self.axes_figure = 0    #сколько на данной фигуре
        self.figures_flushed = 0
        self._next_figure(axes_rows, axes_cols, suptitle)

    def _next_figure(self, axes_rows:int, axes_cols:int, suptitle:str):
        self.figure = plt.figure(figsize=(SUBPLOT_SIZE * axes_cols, SUBPLOT_SIZE * axes_rows), dpi=150, facecolor='white', frameon = False, tight_layout=True)
        #self.figure.suptitle(suptitle)
        p = 0
        for r in range(axes_rows):
            for c in range(axes_cols):
                p += 1
                self.figure.add_subplot(axes_rows, axes_cols, p)
        self.figure_list.append(self.figure)
        self.axes_figure = 0

    def _next_axes(self, title):
        self.axes_group += 1
        self.axes_figure += 1
        #multiplot by itself decides when to flush the figure buffer
        if self.group_full():
            self.save_figures(self.dir, self.format)
            #освобождаем память
            #for fig in self.figure_list:
            #    fig.clf()
            #    del fig
            #SEE 'Working with multiple figures and axes' paragraph in Matplotlib reference
            plt.close('all')
            #del self.figure_list
            # gc.collect(gc.SET_DEBUG)
            self.figure_list = []
            self.axes_group = 0
            self._next_figure(self.axes_rows, self.axes_cols, self.suptitle)
            self._next_axes(title)
        else:
            if self.figure_full():
                self._next_figure(self.axes_rows, self.axes_cols, self.suptitle)
                self.axes_figure = 1
            self.axes_total += 1
            self.axes = self.figure.get_axes()[self.axes_figure-1]
            self.axes.set_title(title)

    def total_empty(self) -> bool:
        return self.axes.total == 0
    def group_full(self) -> bool:
        """How many axes has it drawn since last flush?"""
        return self.axes_group > self.AXES_LIMIT
    def figure_full(self) -> bool:
        return self.axes_figure > (self.axes_rows*self.axes_cols)

    def save_figures(self, dir = '.', format:str= 'pdf', auto:bool = False):
        """Splits big figures with subplots to multiple files.

        :param dir:
        :param format: pdf / png / etc.
        :param auto: when need to just flush the remaining figures
        :return:
        """
        if auto:
            dir = self.dir
            format = self.format
        figure_axes_num = self.axes_rows*self.axes_cols
        for figure in self.figure_list:
            page_file = '{:s}/{:s}{:0>5d}-{:0>5d}.{:s}'.format(dir, self.name, self.figures_flushed + 1, self.figures_flushed + figure_axes_num, format)
            if format=='pdf':
                page_file = PdfPages(page_file)
            figure.savefig(page_file, format=format, facecolor = 'white', transparent=False, dpi=300, bbox_inches='tight')
            if format == 'pdf':
                page_file.close()
            self.figures_flushed = self.figures_flushed + figure_axes_num
        print('{:s} figure group saved.'.format(self.name))

    def samples_plot(self):
        #fig, (ax1, ax2) = plt.subplots(1,2)
        pass

    def event_plot(self):
        #plt.eventplot()
        pass

    def _draw_hull(self, hull, simplices:bool = True):
        try:
            if simplices:
                for simplex in hull.simplices:
                    xs = hull.points[simplex][:,0]
                    ys = hull.points[simplex][:,1]
                    self.axes.plot(xs, ys, 'k-', linewidth=0.72)
            distances, points_max_chord = Utils.get_hull_chord(hull)
            self.axes.plot(points_max_chord[:,0],points_max_chord[:,1], 'b-', linewidth=0.86, alpha=0.44)
            self.desc = self.desc+'\nampl {:.1f}, area {:.0f}'.format(distances.max(), hull.area)
        # если недостаточно точек
        except (QhullError, AttributeError):
            pass
        #self.axes.set_aspect(aspect=1, adjustable='datalim')
    def _draw_suffixes(self, smp, prefix, affix):
        x_min, x_max = smp[SMI_SAMPLE_HEADERS['X']].min(), smp[SMI_SAMPLE_HEADERS['X']].max()
        y_min, y_max = smp[SMI_SAMPLE_HEADERS['Y']].min(), smp[SMI_SAMPLE_HEADERS['Y']].max()
        h_span, v_span = x_max - x_min, y_max - y_min
        h_center, v_center = x_max - h_span/2, y_max - v_span/2
        # делаем кооординаты квадратными, но суффиксы не должны влиять
        if h_span > v_span:
            h_span += 26
            self.axes.set_xlim(left=x_min-13, right=x_max+13)
            self.axes.set_ylim(bottom=v_center+h_span/2, top=v_center-h_span/2) #NOTE reversed Y axis, as on tracker
        elif h_span < v_span:
            v_span += 26
            self.axes.set_xlim(left=h_center-v_span/2, right=h_center+v_span/2)
            self.axes.set_ylim(bottom=y_max+13, top=y_min-13)
        elif h_span == v_span:
            self.axes.set_xlim(left=h_center - 13, right=h_center + 13)
            self.axes.set_ylim(bottom=v_center + 13, top=v_center - 13)
        self.axes.plot(prefix[SMI_SAMPLE_HEADERS['X']], prefix[SMI_SAMPLE_HEADERS['Y']], '-o', color='lightgrey', linewidth=0.1)
        self.axes.plot(affix[SMI_SAMPLE_HEADERS['X']], affix[SMI_SAMPLE_HEADERS['Y']], '-o', color='gray', linewidth=0.1)
    def spatial_fixation(self, smp, props, hull, title=None):
        """Spatial X-Y plot, preferably overlayed on stimulus image.
        Utilizes convex hull.
        #NOTE: Using state machine architecture, like OpenGL."""
        if title == None:
            title = '{:s}{:.0f}'.format(IBDT_CLASS_SHORT[int(props['class_id'])], props['number'])
        self.desc = 'start {:.3f}, dur {:.0f}\nL dia {:.2f}, R dia {:.2f}\nX {:.1f}, Y {:.1f}, var {:.2f}'.format(props['start_s'],
                                                                                                                 props['duration_s'] * 1000,
                                                                                                                 props['dia_l_mean_px'],
                                                                                                                 props['dia_r_mean_px'],
                                                                                                                 props['x_mean_px'],
                                                                                                                 props['y_mean_px'],
                                                                                                                 props['xy_var_px'])
        self._next_axes(title)
        self.axes.set_xlabel(SMI_SAMPLE_HEADERS['X'])
        self.axes.set_ylabel(SMI_SAMPLE_HEADERS['Y'])
        self._draw_hull(hull)
        self._draw_suffixes(smp, smp.prefix, smp.affix)
        self.axes.plot(smp[SMI_SAMPLE_HEADERS['X']], smp[SMI_SAMPLE_HEADERS['Y']], 'o-', color='red', linewidth=0.062)
        self.axes.plot(props['x_mean_px'], props['y_mean_px'], 'bx')
        self.axes.text(0.96, 0.96, self.desc, fontsize=4, horizontalalignment='right', verticalalignment='top', transform=self.axes.transAxes, bbox=dict(facecolor='white', edgecolor='none', alpha=0.74))

    def spatial_saccade(self, smp, props, hull, title=None):
        """Scanpath overlayed on convex hull polygon."""
        if props['class_id'] == IBDT_CLASS['SACCADE']:
            color = 'green'
        elif props['class_id'] == IBDT_CLASS['PURSUIT']:
            color = 'violet'
        if title == None:
            title = '{:s}{:.0f}'.format(IBDT_CLASS_SHORT[int(props['class_id'])], props['number'])
        self.desc = 'start {:.3f}, dur {:.0f}\nvel min {:.2f}, vel max {:.2f}\nvel mean {:.2f}, vel var {:.4f}\ndistance {:.1f}, vel max at {:.2f}'.format(props['start_s'],
                                                                                                                                                           props['duration_s'] * 1000,
                                                                                                                                                           props['velocity_min_pxms'],
                                                                                                                                                           props['velocity_max_pxms'],
                                                                                                                                                           props['velocity_mean_pxms'],
                                                                                                                                                           props['velocity_var_pxms'],
                                                                                                                                                           props['distance_px'],
                                                                                                                                                           props['velocity_max_at_duration'])
        self._next_axes(title)
        #TODO добавить мелкую axes в углу показывающую скорость
        self.axes.set_xlabel(SMI_SAMPLE_HEADERS['X'])
        self.axes.set_ylabel(SMI_SAMPLE_HEADERS['Y'])
        self._draw_hull(hull, simplices=False)
        self._draw_suffixes(smp, smp.prefix, smp.affix)
        self.axes.plot(smp[SMI_SAMPLE_HEADERS['X']], smp[SMI_SAMPLE_HEADERS['Y']], '-o', color=color, linewidth=0.72)
        vel_max = smp['RVelocitySmoothed'].max()
        vel_max_point = smp[smp['RVelocitySmoothed'] == vel_max]
        self.axes.plot(vel_max_point[SMI_SAMPLE_HEADERS['X']], vel_max_point[SMI_SAMPLE_HEADERS['Y']], 'x', color='orange')
        self.axes.text(0.96, 0.96, self.desc, fontsize=4, horizontalalignment='right', verticalalignment='top', transform=self.axes.transAxes, bbox=dict(facecolor='white', edgecolor='none', alpha=0.74))

    def velocity_plot(self, smp, props, title=None):
        if title == None:
            title = 'vel{:.0f}'.format(props['number'])
        self.desc = 'class_id {:s}, smooth {:s}\nstart {:.3f}, dur {:.0f}\nvel max {:.2f}, vel mean {:.2f}'.format(IBDT_CLASS_SHORT[int(props['class_id'])],
                                                                                                                                        'none',#self.metadata['velocity_smooth'],
                                                                                                                                        props['start_s'],
                                                                                                                                        props['duration_s'] * 1000,
                                                                                                                                        props['velocity_max_pxms'],
                                                                                                                                        props['velocity_mean_pxms'])
        try:
            self.desc = self.desc + '\nampl {:.1f} px'.format(props['amplitude_px'])
        except KeyError:
            pass
        self._next_axes(title)
        self.axes.set_xlabel('Time (ms)')
        self.axes.set_ylabel('Velocity (px/ms)')
        self.axes.set_ylim(bottom=-4.0, top=124.0)
        self.axes.plot(smp.prefix['time_0b_ms'], smp.prefix['velocity_pxms'], '-o', color='lightgrey', linewidth=0.1, alpha=0.38)
        self.axes.plot(smp.affix['time_0b_ms'], smp.affix['velocity_pxms'], '-o', color='gray', linewidth=0.1, alpha=0.38)
        self.axes.plot(smp['time_0b_ms'], smp['velocity_pxms'], '-o', color='brown', linewidth=0.58, alpha=0.38)
        #self.axes.plot(smp.prefix['time_0b_ms'], smp.prefix['RVelocitySmoothed'], '-o', color='lightgrey', linewidth=0.1)
        #self.axes.plot(smp.affix['time_0b_ms'], smp.affix['RVelocitySmoothed'], '-o', color='gray', linewidth=0.1)
        #self.axes.plot(smp['time_0b_ms'], smp['RVelocitySmoothed'], '-o', color='orange', linewidth=0.88)
        self.axes.text(0.96, 0.96, self.desc, fontsize=4, horizontalalignment='right', verticalalignment='top', transform=self.axes.transAxes, bbox=dict(facecolor='white', edgecolor='none', alpha=0.74))

    def combi_plot(self):
        """Both temporal and spatial plots stacked on top of each other, plus pupil diameter, eye velocity and acceleration."""
        pass