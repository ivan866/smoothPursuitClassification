import math, cmath
from datetime import datetime, date, timedelta
import angles
import numpy as np
from scipy.spatial import ConvexHull
from scipy.spatial.distance import pdist, squareform
import pandas
from pandas import Series

POSSIBLE_TIME_STRF = ['%H:%M:%S.%f', '%M:%S.%f', '%M:%S', '%S.%f', '%S']
TIME_COLUMN_NAMES = ['Time', 'Recording timestamp', 'start_s']
STATUS_ENUM = {'OK': 'ok',
               'WARNING': 'warning',
               'ERROR': 'error'}
COORDINATES_MODE = {'FROM_CARTESIAN':0,
                    'FROM_POLAR':1}

def get_now_string():
    return datetime.now().strftime('%Y-%m-%d %H_%M_%S')

#TIME formatting methods
def _guess_time_format(val:object) -> str:
    """Helper method to determine the time strf string.
    :param val: Time string to try to parse.
    :return: Format string.
    """
    if type(val) is not str:
        val = str(val)
    for fmt in POSSIBLE_TIME_STRF:
        try:
            datetime.strptime(val, fmt)
        except ValueError:
            try:
                pandas.to_datetime(val, unit='s')
            except ValueError:
                continue
            break
        break
    #print('Time format of ' + val + ' string is guessed as ' + fmt + '.')
    return fmt

def locate_specific_column(names, repertoir:list = TIME_COLUMN_NAMES) -> int:
    """On which position does this dataframe contain needed column?

    :param names: pandas index object
    :param repertoir: which column to search (list)
    :return: index of match
    """
    names = list(names)
    for r in repertoir:
        try:
            i = names.index(r)
            return i
        except ValueError:
            pass

#FIXME valueerror if format =s.f and s>61
def parse_time(val:object = 0) -> timedelta:
    """Helper method to convert time strings to datetime objects.
    Agnostic of time string format.
    :param val: Time string or float.
    :return: timedelta object.
    """
    val = str(val)
    fmt = _guess_time_format(val)
    try:
        parsed = datetime.strptime(val, fmt)
    except ValueError:
        parsed = pandas.to_datetime(val, unit='s')
    return datetime.combine(date.min,parsed.time()) - datetime.min

def parse_time_v(data:Series) -> Series:
    """Vectorized version of parseTime method.
    :param data: pandas Series object.
    :return: Same object with values converted to timedelta.
    """
    if data.name in TIME_COLUMN_NAMES:
        return pandas.to_timedelta(data.astype(float), unit='s')
    else:
        return pandas.to_datetime(data.astype(str), infer_datetime_format=True) - date.today()

#CONVERSION methods
def get_separation(x1:float, y1:float, x2:float, y2:float, z:float=None, mode:int=None) -> float:
    """Returns angular separation between two angles on a unit sphere.
    :param x1:
    :param y1:
    :param x2:
    :param y2:
    :param z: depth, in mm; not needed if mode polar
    :param mode: whether coordinates are passed in mm or degrees
    :return: angle (arc) in degrees
    """
    #does it account for gimbal lock? Use QUATERNIONS!
    if mode == COORDINATES_MODE['FROM_CARTESIAN']:
        lon1 = cmath.polar(complex(x1, z))[1]-math.pi/2
        lat1 = cmath.polar(complex(y1, z))[1]-math.pi/2
        lon2 = cmath.polar(complex(x2, z))[1]-math.pi/2
        lat2 = cmath.polar(complex(y2, z))[1]-math.pi/2
        sep = angles.sep(lon1,lat1, lon2,lat2)
    elif mode == COORDINATES_MODE['FROM_POLAR']:
        sep = angles.sep(math.radians(x1),math.radians(y1), math.radians(x2),math.radians(y2))
    else:
        raise ValueError('COORDINATES_MODE unknown.')
    return math.degrees(sep)

def get_hull_chord(hull:ConvexHull) -> (np.array, np.array):
    """Returns points belonging to the most long chord of the convex hull.
    :param hull: convex hull
    :return: array of distances (crosstab), array of 2 points (longest chord)
    """
    distances = squareform(pdist(hull.points[hull.vertices], metric='euclidean'))
    distance_max = distances.max()
    #FIXME deprecated in pandas 1.0
    index_max_vert = (distances == distance_max).nonzero()[0]
    index_max_point = hull.vertices[[index_max_vert[0], index_max_vert[1]]]
    points_max_chord = hull.points[[index_max_point[0], index_max_point[1]]]
    return (distances, points_max_chord)
