import numpy as np
from numpy import ndarray
from pandas import Series, DataFrame
#used for event amplitude calculation, also allows geometric center of dispersion cloud
from scipy.spatial import ConvexHull
from scipy.spatial.qhull import QhullError
from algo.GazeData import IBDT_CLASS
from utils import Utils
from utils.Utils import COORDINATES_MODE
from parsers.DataReader import SMI_SAMPLE_HEADERS

FIND_MODE = {'BY_ROW_INDEX':0,
             'BY_TIMESTAMP':1}
#defines how event list is output, either each sample is tagged by its event class id (commonly used for input to machine learning classifiers, for confusion matrices and kappa-levels),
#  or time ranges of events are found first + additional event properties such as mean speed (better suits for statistics)
OUTPUT_LAYOUT = {'EACH_SAMPLE': 0,
                 'EVENT_INTERVALS': 1}
OUTPUT_PARSER = {'ONE_FILE':0,  #like SMI Events.txt with different headers for each event type
                 'SEPARATE_EVENT_FILES':5}  #every event type just goes to its own file with its unique header
OUTPUT_TYPE = {'SPC_IBDT_RAW': 0,
               'SMI_EVENT_EACH_SAMPLE': 3}
#TODO ?
SMI_EVENT_HEADERS = {}

#TODO import profile
#  profile.run('main()')
class EventUtils:
    """Special class containing and manipulating output of classification algos.
    E.g., getting result filtered, and props calculated
    """
    def __init__(self, algo_output:DataFrame, type:int):
        self.result = algo_output
        self.type = type

    #TODO update Pandas
    def find_event_ranges(self, input_data:ndarray, diff_by:str, tag_or_not:bool = False) -> ndarray:
        """Locates changes of a specific column value and makes intervals out of it.
        Also tags intervals by starting and ending indices.
        :param input_data:
        :param diff_by: column name to diff array by
        :param tag_or_not: add interval borders or not
        :return: processed array
        """
        class_diff = np.hstack((1, np.diff(input_data[diff_by])))
        indices = class_diff != 0
        #TODO to_numpy().nonzero()
        border_start:int = indices.nonzero()[0]
        border_end:int = np.hstack((border_start[1:]-1, input_data.shape[0]-1))
        result = input_data[indices]
        #FIXME abs indices
        result.insert(1, 'SampleIndexStart', border_start)
        result.insert(2, 'SampleIndexEnd', border_end)
        return result
    def _find_corresponding_data_range(self, range_start:float, range_end:float, aux_data, column_to_search=None, find_mode:int=FIND_MODE['BY_ROW_INDEX']) -> DataFrame:
        """Finds range of i.e. samples, corresponding to i.e. single fixation.
        Assumes data_object is DataFrame.
        :param column_to_search: by which column to filter DataFrame
        :param range_start: absolute values to select range unto
        :param range_end:
        :param aux_data: DataFrame to search column in
        :return: subset data_object"""
        if find_mode==FIND_MODE['BY_ROW_INDEX']:
            return aux_data.iloc[int(range_start):int(range_end)]
        elif find_mode==FIND_MODE['BY_TIMESTAMP']:
            return aux_data.loc[(aux_data[column_to_search] >= range_start) & (aux_data[column_to_search] < range_end)]
    def _add_index(self, data:DataFrame):
        bool_fix = data['class_id'] == IBDT_CLASS['FIXATION']
        bool_sac = data['class_id'] == IBDT_CLASS['SACCADE']
        bool_pur = data['class_id'] == IBDT_CLASS['PURSUIT']
        bool_noi = data['class_id'] == IBDT_CLASS['NOISE']
        bool_und = data['class_id'] == IBDT_CLASS['UNDEF']
        data.insert(1, 'number', [0 for i in range(data.shape[0])])
        #BUG nonzero() does not exist in Pandas 1.0
        data.loc[bool_fix, 'number'] = [n + 1 for n in range(bool_fix.nonzero()[0].shape[0])]
        data.loc[bool_sac, 'number'] = [n + 1 for n in range(bool_sac.nonzero()[0].shape[0])]
        data.loc[bool_pur, 'number'] = [n + 1 for n in range(bool_pur.nonzero()[0].shape[0])]
        data.loc[bool_noi, 'number'] = [n + 1 for n in range(bool_noi.nonzero()[0].shape[0])]
        data.loc[bool_und, 'number'] = [n + 1 for n in range(bool_und.nonzero()[0].shape[0])]
    #TODO converter to insert SMI IDF Converter output here
    #  NOTE meanwhile, gets parsed in R and fed in as regular csv
    # TODO change all loc[...] to query('...')
    #TODO add geom_center_x / y
    def generate_event_props(self, aux_data:DataFrame, output_layout:int = OUTPUT_LAYOUT['EVENT_INTERVALS']) -> DataFrame:
        """Parses result of classification, finds event time borders, calculates durations.
        :param aux_data: data containing samples, in this case
        :param output_layout: see description for this enum
        :return: dataframe of events list - fixations, saccades, smooth pursuits
        """
        print('Performing property calculation for each classified event.')
        #TODO replace with logger debug msgs
        #fix_n=0
        #sac_n=0
        if self.type==OUTPUT_TYPE['SPC_IBDT_RAW']:
            out_ar = []
            for pt in self.result:
                out_ar.append([pt.base.ts / 1000, pt.base.classification])
            output = DataFrame(out_ar, columns=['start_s', 'class_id'])
        elif self.type==OUTPUT_TYPE['SMI_EVENT_EACH_SAMPLE']:
            output = self.result.rename({'timestamp':'start_s'}, axis=1)
        if output_layout==OUTPUT_LAYOUT['EACH_SAMPLE']:
            return output
        elif output_layout==OUTPUT_LAYOUT['EVENT_INTERVALS']:
            print('NOTE: Both SampleStartIndex and SampleEndIndex fields are inclusive, but Duration extends till the following event start.')
            nonzero = self.find_event_ranges(output, diff_by='class_id', tag_or_not=True)
            evt_fulldata = DataFrame()
            self._add_index(nonzero)
            nonzero.insert(1, 'duration_s', np.hstack((np.diff(nonzero['start_s']), np.nan)))
            assert output.shape[0] == aux_data.shape[0], '[ERROR] Aux data provided does not match classifier result.'
            #FIXME concurrent on all threads, or use multiple script copies
            for index, evt in nonzero.iterrows():
                #NOTE python >=3.6 preserves dict order
                #TODO sac curvature, angle spectre plot
                props = {k:evt[k] for k in list(evt.keys())}
                #NOTE indexend needs +1 because pandas iloc method takes opened range
                smp = self._find_corresponding_data_range(evt.SampleIndexStart, evt.SampleIndexEnd + 1, aux_data, FIND_MODE['BY_ROW_INDEX'])
                zero_time = smp[SMI_SAMPLE_HEADERS['TIMESTAMP']].iloc[0]
                #TODO Dia, Sac direction dynamics
                try:
                    #props['dia_l_min_mm'] = smp[DATA_HEADERS_SMI['DIA_L']].min()
                    #props['dia_l_max_mm'] = smp[DATA_HEADERS_SMI['DIA_L']].max()
                    props['dia_l_mean_px'] = ((smp['L Dia X [px]'] + smp['L Dia Y [px]'])/2).mean()
                    props['dia_l_mean_mm'] = smp[SMI_SAMPLE_HEADERS['DIA_L']].mean()
                    #props['dia_r_min_mm'] = smp[DATA_HEADERS_SMI['DIA_R']].min()
                    #props['dia_r_max_mm'] = smp[DATA_HEADERS_SMI['DIA_R']].max()
                    props['dia_r_mean_px'] = ((smp['R Dia X [px]'] + smp['R Dia Y [px]'])/2).mean()
                    props['dia_r_mean_mm'] = smp[SMI_SAMPLE_HEADERS['DIA_R']].mean()
                except KeyError:
                    pass
                props['xy_var_px'] = Series((smp[SMI_SAMPLE_HEADERS['X']].var(), smp[SMI_SAMPLE_HEADERS['Y']].var())).mean()
                props['xy_var_deg'] = Series((smp['RPORXDeg'].var(), smp['RPORYDeg'].var())).mean()
                # speed is scalar, velocity is a vector, but I'm used to it
                vel_max = smp['RVelocity'].max()
                vel_max_point = smp[smp['RVelocity'] == vel_max]
                vel_max_time = vel_max_point['Time'].iloc[0]
                smp_before_vel_max = smp[smp['Time'] < vel_max_time]
                props['velocity_min_pxms'] = smp['velocity_pxms'].min()
                props['velocity_max_pxms'] = smp['velocity_pxms'].max()
                props['velocity_mean_pxms'] = smp['velocity_pxms'].mean()
                props['velocity_min_degs'] = smp['RVelocitySmoothed'].min()
                props['velocity_max_degs'] = vel_max
                props['velocity_mean_degs'] = smp['RVelocitySmoothed'].mean()
                props['velocity_var_pxms'] = smp['velocity_pxms'].var()
                props['velocity_var_degs'] = smp['RVelocitySmoothed'].var()
                if smp.shape[0]>1:
                    props['distance_px'] = smp['distance_px'].iloc[1:].sum()
                else:
                    props['distance_px'] = np.nan
                hull = None
                try:
                    hull = ConvexHull(np.array(smp[[SMI_SAMPLE_HEADERS['X'], SMI_SAMPLE_HEADERS['Y']]]))
                    distances, points_max_chord = Utils.get_hull_chord(hull)
                    point1 = smp.loc[(smp[SMI_SAMPLE_HEADERS['X']] == points_max_chord[0, 0]) & (smp[SMI_SAMPLE_HEADERS['Y']] == points_max_chord[0, 1])].iloc[0]
                    point2 = smp.loc[(smp[SMI_SAMPLE_HEADERS['X']] == points_max_chord[1, 0]) & (smp[SMI_SAMPLE_HEADERS['Y']] == points_max_chord[1, 1])].iloc[0]  #бывает множественное совпадение если координаты 0,0
                    props['amplitude_px'] = distances.max()
                    if self.type == OUTPUT_TYPE['SPC_IBDT_RAW']:
                        props['amplitude_deg'] = Utils.get_separation(point1['RPORXMm'], point1['RPORYMm'], point2['RPORXMm'], point2['RPORYMm'], z=aux_data.metadata['head_distance_mm'], mode=COORDINATES_MODE['FROM_CARTESIAN'])
                    elif self.type == OUTPUT_TYPE['SMI_EVENT_EACH_SAMPLE']:
                        props['amplitude_deg'] = Utils.get_separation(point1['RPORXDeg'], point1['RPORYDeg'], point2['RPORXDeg'], point2['RPORYDeg'], mode=COORDINATES_MODE['FROM_POLAR'])
                    props['hull_area_pxsq'] = hull.area
                except (QhullError, AttributeError):
                    pass
                if evt.class_id == IBDT_CLASS['FIXATION']:
                    #fix_n+=1
                    #print('fix',fix_n)
                    props['x_mean_px'] = smp[SMI_SAMPLE_HEADERS['X']].mean()
                    props['y_mean_px'] = smp[SMI_SAMPLE_HEADERS['Y']].mean()
                    props['x_mean_deg'] = smp['RPORXDeg'].mean()
                    props['y_mean_deg'] = smp['RPORYDeg'].mean()
                elif evt.class_id == IBDT_CLASS['SACCADE'] or evt.class_id == IBDT_CLASS['PURSUIT']:
                    #sac_n += 1
                    #print('sac',sac_n)
                    max_index = smp.shape[0]-1
                    props['x_start_px'] = smp[SMI_SAMPLE_HEADERS['X']].iloc[0]
                    props['y_start_px'] = smp[SMI_SAMPLE_HEADERS['Y']].iloc[0]
                    props['x_end_px'] = smp[SMI_SAMPLE_HEADERS['X']].iloc[max_index]
                    props['y_end_px'] = smp[SMI_SAMPLE_HEADERS['Y']].iloc[max_index]
                    props['x_start_deg'] = smp['RPORXDeg'].iloc[0]
                    props['y_start_deg'] = smp['RPORYDeg'].iloc[0]
                    props['x_end_deg'] = smp['RPORXDeg'].iloc[max_index]
                    props['y_end_deg'] = smp['RPORYDeg'].iloc[max_index]
                    #NOTE duration counts till next event begins, therefore this prop never reaches 1.00
                    props['velocity_max_at_duration'] = (vel_max_time - zero_time) / props['duration_s']
                    if smp_before_vel_max.shape[0] > 1:
                        props['velocity_max_at_distance'] = smp_before_vel_max['distance_px'].sum() / props['distance_px']
                    else:
                        props['velocity_max_at_distance'] = np.nan
                elif evt.class_id == IBDT_CLASS['NOISE'] and self.type==OUTPUT_TYPE['SPC_IBDT_RAW']:
                    raise NotImplementedError('ALERT: NOISE event type has been classified!')
                elif evt.class_id == IBDT_CLASS['NOISE'] and self.type == OUTPUT_TYPE['SMI_EVENT_EACH_SAMPLE']:
                    pass
                elif evt.class_id == IBDT_CLASS['UNDEF']:
                    pass
                else:
                    raise ValueError('Unexpected event type: {}'.format(evt.class_id))
                    pass
                evt_fulldata = evt_fulldata.append(Series(props), ignore_index=True)
            return evt_fulldata
        else:
            raise ValueError('OUTPUT_MODE unknown.')

    def apply_bad_event_filters(self, evt_fulldata:DataFrame) -> tuple:
        """Filters given data by predefined criteria.
        Returns both good and bad event collections.
        """
        #TODO set difference between full and good = bad
        total_n = evt_fulldata.shape[0]
        print('Applying threshold filters to events...')
        print('fix / sac / pur / noi: {:.0f}% / {:.0f}% / {:.0f}% / {:.0f}%'.format(evt_fulldata.query('class_id==0').shape[0] / total_n * 100, evt_fulldata.query('class_id==1').shape[0] / total_n * 100, evt_fulldata.query('class_id==2').shape[0] / total_n * 100, evt_fulldata.query('class_id==3').shape[0] / total_n * 100))
        fix_filter1 = evt_fulldata.query('class_id==0 and duration_s > 0.08')
        fix_filter2 = evt_fulldata.query('class_id==0 and velocity_max_degs <= 30')
        fix_filter3 = evt_fulldata.query('class_id==0 and amplitude_deg <= 2.5')
        # doing boolean intersection (inner join by default)
        sac_filter1 = evt_fulldata.query('class_id==1 and duration_s > 0.018 and duration_s < 0.08')
        sac_filter2 = evt_fulldata.query('class_id==1 and velocity_mean_degs > 30')
        sac_filter3 = evt_fulldata.query('class_id==1 and velocity_max_degs < 800')
        sac_filter4 = evt_fulldata.query('class_id==1 and amplitude_deg > 2.5 and amplitude_deg < 16')
        sac_filter5 = evt_fulldata.query('class_id==1 and (distance_px - amplitude_px) < 100')
        sac_filter6 = evt_fulldata.query('class_id==1 and hull_area_pxsq < (amplitude_px*60)')
        pur_filter1 = evt_fulldata.query('class_id==2 and duration_s >= 0.08 and duration_s < 2.5')
        pur_filter2 = evt_fulldata.query('class_id==2 and velocity_mean_degs <= 30')
        pur_filter3 = evt_fulldata.query('class_id==2 and velocity_max_degs < 400')
        pur_filter4 = evt_fulldata.query('class_id==2 and amplitude_deg < 16')
        print('fix filters: {:.1f}% / {:.1f}% / {:.1f}% removed'.format(100 - fix_filter1.shape[0] / total_n * 100, 100-fix_filter2.shape[0]/total_n*100, 100-fix_filter3.shape[0]/total_n*100))
        print('sac filters: {:.1f}% / {:.1f}% / {:.1f}% / {:.1f}% / {:.1f}% / {:.1f}% removed'.format(100-sac_filter1.shape[0]/total_n*100, 100-sac_filter2.shape[0]/total_n*100, 100-sac_filter3.shape[0]/total_n*100, 100-sac_filter4.shape[0]/total_n*100, 100-sac_filter5.shape[0]/total_n*100, 100-sac_filter6.shape[0]/total_n*100))
        print('pur filters: {:.1f}% / {:.1f}% / {:.1f}% / {:.1f}% removed'.format(100 - pur_filter1.shape[0] / total_n * 100, 100 - pur_filter2.shape[0] / total_n * 100, 100 - pur_filter3.shape[0] / total_n * 100, 100 - pur_filter4.shape[0] / total_n * 100))
        fix_intersect = fix_filter1.merge(fix_filter2).merge(fix_filter3)
        sac_intersect = sac_filter1.merge(sac_filter2).merge(sac_filter3).merge(sac_filter4).merge(sac_filter5).merge(sac_filter6)
        pur_intersect = pur_filter1.merge(pur_filter2).merge(pur_filter3).merge(pur_filter4)
        print('fix / sac / pur filter combined: {:.2f}% / {:.2f}% / {:.2f}% removed'.format(100-fix_intersect.shape[0]/total_n*100, 100-sac_intersect.shape[0]/total_n*100, 100-pur_intersect.shape[0]/total_n*100))
        filtered_data = fix_intersect.append(sac_intersect).append(pur_intersect).sort_values(by='start_s')
        #TODO
        # taking boolean difference
        #start_bad = set(evt_fulldata['start_s']) - set(filtered_data['start_s'])
        #bad_data = evt_fulldata.join(filtered_data, how='outer', rsuffix='bad_')
        #evt_fulldata.index.difference(filtered_data)
        #NOTE print df2[~df2.isin(df1).all(1)]
        #           df1[~df1.index.isin(df2.index)]
        #  print df2[(df2!=df1)].dropna(how='all')
        #  print df2[~(df2==df1)].dropna(how='all')
        #NOTE merge_df = df1.merge(df3, how='left', indicator=True)
        #  столбец _merge > left_only - то что надо
        #print('filtered / bad: {:.0f}% / {:.0f}%'.format(filtered_data.shape[0] / total_n * 100, bad_data.shape[0] / total_n * 100))
        return (filtered_data)#, bad_data)
