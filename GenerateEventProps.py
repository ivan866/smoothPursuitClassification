#!/usr/bin/env python3
# -*- coding: utf8 -*-
import os, sys, argparse
import numpy as np
import pandas as pd
from utils import Utils
from utils.EventUtils import EventUtils, OUTPUT_LAYOUT, OUTPUT_TYPE

DIR_NAME = 'EVT_PROPS'
parser = argparse.ArgumentParser(description='Intended for getting event props from parsed SMI *Events.txt (converted to EACH_SAMPLE view).')
parser.add_argument('--smp-autotagged', type=str, help='Processed *Samples.txt; used for parsing the smp data range and extracting many props.')
parser.add_argument('--evt-each-sample', type=str, help='Path to events classified by "IDF Event Detector" and parsed through R script.')
args = parser.parse_args()
os.makedirs(DIR_NAME, exist_ok=True)

smp_autotagged = pd.read_csv(args.smp_autotagged)
evt_each_sample = pd.read_csv(args.evt_each_sample, sep=',', error_bad_lines=True)
e_u = EventUtils(evt_each_sample, type=OUTPUT_TYPE['SMI_EVENT_EACH_SAMPLE'])
evt_fulldata = e_u.generate_event_props(smp_autotagged, OUTPUT_LAYOUT['EVENT_INTERVALS'])
total_n = evt_fulldata.shape[0]
print('fix / sac / pur / noi / und: {:.0f}% / {:.0f}% / {:.0f}% / {:.0f}% / {:.0f}%'.format(evt_fulldata.query('class_id==0').shape[0] / total_n * 100, evt_fulldata.query('class_id==1').shape[0] / total_n * 100, evt_fulldata.query('class_id==2').shape[0] / total_n * 100, evt_fulldata.query('class_id==3').shape[0] / total_n * 100, evt_fulldata.query('class_id==4').shape[0] / total_n * 100))
#FIXME code copied from DataExporter
#TODO trial is not tagged
evt_fulldata = evt_fulldata.reindex(['start_s','duration_s','trial','Id','number','class_id',
                                     'SampleIndexStart','SampleIndexEnd',
                                     'dia_l_mean_px','dia_r_mean_px','dia_l_mean_mm','dia_r_mean_mm',
                                     'x_start_px','y_start_px','x_end_px','y_end_px',
                                     'x_mean_px','y_mean_px','xy_var_px',
                                     'x_start_deg','y_start_deg','x_end_deg','y_end_deg',
                                     'x_mean_deg','y_mean_deg','xy_var_deg',
                                     'velocity_min_pxms','velocity_max_pxms','velocity_mean_pxms','velocity_var_pxms',
                                     'velocity_min_degs','velocity_max_degs','velocity_mean_degs','velocity_var_degs',
                                     'velocity_max_at_duration','velocity_max_at_distance',
                                     'distance_px','amplitude_px','amplitude_deg',
                                     'hull_area_pxsq'], axis=1)
evt_fulldata.to_csv('{:s}/{:s}_{:s}_props.csv'.format(DIR_NAME, Utils.get_now_string(), os.path.splitext(os.path.basename(args.evt_each_sample))[0]), index=False)

print('Event props written to {}.'.format(DIR_NAME))
sys.exit()