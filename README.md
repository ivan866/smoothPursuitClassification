# Smooth Pursuit Classification

A Python 3 port of I-BDT classifier (cpp version) for smooth pursuits, saccades, fixations and noise by Santini, Fuhl, Kubler et al.

Reads SMI eye tracker data.

Includes R converters to and from following formats: ARFF, Dikablis Pro journal, Tobii Glasses 2 .json + fake folder structure generator, SMI Samples.