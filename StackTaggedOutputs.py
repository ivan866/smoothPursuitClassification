#TODO fastest way to do this is by binary combining [cat file1 file2] files with only first one having the header line
import os, sys, argparse
from datetime import datetime
from pathlib import Path
from utils import Utils
import pandas as pd

#TODO sort by Id and Time
parser = argparse.ArgumentParser(description='This script reads, stacks [like base::rbind()] and outputs input filelist.')
parser.add_argument('--list', type=str, help='Path to filelist.')
args = parser.parse_args()
with open(args.list, encoding='UTF-8') as l:
    fs = [f for f in l.readlines()]
now = Utils.get_now_string()
fname = '{0}/../STACKED_{1}_{2}'.format(os.path.dirname(fs[0]), now, os.path.basename(fs[0]).rstrip('\n'))
header = True
print('Estimated file size: {:.3f} MB'.format(len(fs) * Path(fs[0].rstrip('\n')).stat().st_size / 1024**2))
for f in fs:
    taggeddata = pd.read_csv(f.rstrip('\n'), sep=',', error_bad_lines=True)
    taggeddata.to_csv(fname, sep=',', header=header, index=False, mode='a')
    header = False
print('Done. Stacked.')
sys.exit()