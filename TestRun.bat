REM python -m unittest discover -s tests
python SmoothPursuitClassification.py -s=./TestSettings.xml --algo=ibdt
REM python SmoothPursuitClassification.py -s=./TestSettings.xml --algo=ibdt --plots spatial-event velocity

REM python SmoothPursuitClassification.py -s=./TestSettings.xml --smooth=savgol --algo=ivt
REM python SmoothPursuitClassification.py -s=./TestSettings.xml --smooth=1dspline --algo=ivt --plots spatial-event
REM python SmoothPursuitClassification.py -s=./TestSettings.xml --smooth=conv --algo=ivt

REM #NOTE not implemented
REM python SmoothPursuitClassification.py -s=./TestSettings.xml --smooth=1dspline --algo=idt --classifier=blstm --backend=tf --plots=velocity