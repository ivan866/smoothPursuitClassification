#!/usr/bin/env python3
# -*- coding: utf8 -*-
import os
from datetime import datetime
import time
import xml.etree.ElementTree as ET
from pandas import DataFrame
from utils.Utils import STATUS_ENUM
from utils.SettingsReader import SettingsReader
from utils.EventUtils import OUTPUT_LAYOUT

class DataExporter():
    """Helper class that writes to files some particularly data channels useful for further data analysis."""
    def __init__(self, main):
        self.main = main
        self.settings_reader = main.settings_reader
        self.save_dir= ''
        #NOTE these columns do NOT get exported anywhere
        self.cols_unperceptable=['TimestampZeroBased', 'Timedelta', 'Trial', 'Record tag', 'Type', 'Timing', 'Latency', 'L Validity', 'R Validity', 'pupil_validity', 'RPORXMm', 'RPORYMm', 'RPORXMmSmoothed', 'RPORYMmSmoothed']

    def create_dir(self, prefix:str = 'OUTPUT', dry_run:bool = False) -> str:
        """Creates timestamped directory to save data into.
        :param prefix: directory name prefix.
        :param dry_run: whether to actually create the dir or just generate the path
        :return: Directory path str.
        """
        if self.settings_reader._check(full=True):
            while True:
                now = datetime.now().strftime('%Y-%m-%d %H_%M_%S')
                self.save_dir = '{0}/{1}_{2}'.format(self.settings_reader.data_dir, str(prefix), now)
                if not dry_run:
                    try:
                        os.makedirs(self.save_dir)
                        break
                    #can happen if multiple scripts running simultaneously [race condition]
                    except FileExistsError:
                        print('WARNING: Directory with this date / time combination exists. Retrying in 2 sec...')
                        time.sleep(2)
            date_tag = ET.Element('date')
            date_tag.text = now
            self.settings_reader.settings.append(date_tag)
            return self.save_dir
        else:
            raise ValueError('No settings found')

    def copy_meta(self, save_dir:str = '') -> None:
        """Writes settings and report to previously created save directory.
        :param save_dir:
        :return:
        """
        if self.settings_reader._check():
            if save_dir:
                meta_dir = save_dir
            else:
                meta_dir = self.save_dir
            self.settings_reader.save(meta_dir)
            self.main.print_to_out('Current settings copied to output.')
            self.main.save_console(meta_dir)

    def export_CSV(self, multi_data:object, format:str= 'csv') -> None:
        """Writes all data to CSV files.
        :param multi_data:
        :param format:
        :return:
        """
        if self.settings_reader._check() and multi_data._check():
            self.main.print_to_out('Exporting data channels to {0}.'.format(format.upper()))
            save_dir = self.create_dir()
            for type in multi_data.multi_data.keys():
                stacked = DataFrame()
                #FIXME extension should generate by itself
                file = '{0}/{1}_autotagged.{2}'.format(save_dir, type, format)
                for (channel, id) in multi_data.gen_channel_ids(channel=type):
                    #TODO switch to tagging mode if more than 1 id in settings
                    #data = multi_data.get_channel_and_tag(channel, id,   block='interval', format='dataframe',   ignoreEmpty=False)
                    data = multi_data.get_channel_by_id(channel, id, format='dataframe')
                    stacked = stacked.append(data, sort=False)
                #TODO elegant OOP: column output precision and order should be carried along with the column Series itself; then a private method carries out the formatting of 'stacked' df
                if format == "csv" and len(stacked):
                    stacked.drop(self.cols_unperceptable, axis=1, inplace=True, errors='ignore') #дропается только часть либо все возможные
                    stacked = stacked.round(6)
                    if channel == 'smi-samples':
                        #TODO noop
                        stacked.style.format({'Time':'{:.6f}','trial duration':'{:.6f}',
                                              'RPORXMm':'{:.2f}','RPORXDeg':'{:.2f}',
                                              'RPORYMm':'{:.2f}','RPORYDeg':'{:.2f}',
                                              'RPORXPxSmoothed':'{:.4f}','RPORXMmSmoothed':'{:.2f}','RPORXDegSmoothed':'{:.2f}',
                                              'RPORYPxSmoothed':'{:.4f}','RPORYMmSmoothed':'{:.2f}','RPORYDegSmoothed':'{:.2f}',
                                              'RVelocity':'{:.3f}','RVelocitySmoothed':'{:.3f}',
                                              'distance_px':'{:.3f}','velocity_pxms':'{:.3f}'})
                    elif channel == 'smi-messages':
                        stacked.style.format({'Time':'{:.6f}'})
                    elif channel == 'ibdt-events':
                        stacked.style.format({'start_s':'{:.6f}','duration_s':'{:.6f}',
                                              'number':'{:.0f}','class_id':'{:.0f}',
                                              'SampleIndexStart':'{:.0f}','SampleIndexEnd':'{:.0f}',
                                              'dia_l_mean_mm':'{:.2f}','dia_r_mean_mm':'{:.2f}','dia_l_mean_px':'{:.4f}','dia_r_mean_px':'{:.4f}',
                                              'distance_px':'{:.1f}',
                                              'x_start_deg':'{:.1f}','y_start_deg':'{:.1f}','x_end_deg':'{:.1f}','y_end_deg':'{:.1f}',
                                              'x_mean_deg':'{:.1f}','y_mean_deg':'{:.1f}','xy_var_deg':'{:.3f}',
                                              'x_start_px':'{:.4f}','y_start_px':'{:.4f}','x_end_px':'{:.4f}','y_end_px':'{:.4f}',
                                              'x_mean_px':'{:.1f}','y_mean_px':'{:.1f}','xy_var_px':'{:.2f}',
                                              'velocity_max_at_distance':'{:.2f}','velocity_max_at_duration':'{:.2f}',
                                              'velocity_min_degs':'{:.1f}','velocity_max_degs':'{:.1f}','velocity_mean_degs':'{:.1f}','velocity_var_degs':'{:.0f}',
                                              'velocity_min_pxms':'{:.3f}','velocity_max_pxms':'{:.3f}','velocity_mean_pxms':'{:.3f}','velocity_var_pxms':'{:.3f}',
                                              'amplitude_deg':'{:.2f}','amplitude_px':'{:.1f}',
                                              'hull_area_pxsq':'{:.2f}'})
                        #NOTE 'Id' column case-sensitive and inconsistent
                        #querying multi-data object for its attributes, +private method
                        if multi_data._attributes['output_layout'] == OUTPUT_LAYOUT['EACH_SAMPLE']:
                            stacked = stacked.reindex(['start_s', 'trial', 'Id', 'class_id'], axis=1)
                        elif multi_data._attributes['output_layout'] == OUTPUT_LAYOUT['EVENT_INTERVALS']:
                            stacked = stacked.reindex(['start_s','duration_s','trial','Id','number','class_id',
                                                       'SampleIndexStart','SampleIndexEnd',
                                                       'dia_l_mean_px','dia_r_mean_px','dia_l_mean_mm','dia_r_mean_mm',
                                                       'x_start_px','y_start_px','x_end_px','y_end_px',
                                                       'x_mean_px','y_mean_px','xy_var_px',
                                                       'x_start_deg','y_start_deg','x_end_deg','y_end_deg',
                                                       'x_mean_deg','y_mean_deg','xy_var_deg',
                                                       'velocity_min_pxms','velocity_max_pxms','velocity_mean_pxms','velocity_var_pxms',
                                                       'velocity_min_degs','velocity_max_degs','velocity_mean_degs','velocity_var_degs',
                                                       'velocity_max_at_duration','velocity_max_at_distance',
                                                       'distance_px','amplitude_px','amplitude_deg',
                                                       'hull_area_pxsq'], axis=1)
                    stacked.to_csv(file, sep=',', header=True, index=False, mode='w')
            #----
            self.main.print_to_out('Done.', status=STATUS_ENUM['OK'])
            self.main.print_to_out('Data blocks tagged.')
            #self.main.print_to_out('Note: "Trial" column is by iViewX API, but "trial" column is auto-parsed.')
            self.copy_meta()