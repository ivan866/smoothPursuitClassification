#!/usr/bin/env python3
# -*- coding: utf8 -*-
import os, re, math
import xml.etree.ElementTree as ET
import pandas as pd
from utils import Utils
from utils.Utils import COORDINATES_MODE, STATUS_ENUM

#unfortunately, columns indices are not reliable here, SMI allows excluding
SMI_SAMPLE_HEADERS = {'TIMESTAMP': 'Time',
                    'TYPE_ID':'Type',
                    'X':'R POR X [px]',
                    'Y':'R POR Y [px]',
                    'DIA_L':'L Mapped Diameter [mm]',
                    'DIA_R':'R Mapped Diameter [mm]',
                    '_DUMMY': -1}

class DataReader():
    """Helper class that reads and parses eyetracking data (currently SMI only)."""
    def __init__(self, main):
        self.main = main

    def read(self, settings_reader:object, multi_data:object) -> None:
        """Actual data parsing code.
        Depends on pandas module.
        :param settings_reader: SettingsReader object to get xml settings tree from.
        :param multi_data: MultiData object to write into.
        :return:
        """
        #self.main.logger.debug('reading data...')
        if settings_reader._check():
            settings_reader.read()
        else:
            return
        multi_data.reset()
        try:
            self.read_SMI_samples(settings_reader, multi_data)
            if settings_reader._check(full=True) and multi_data._check():
                self.main.print_to_out('All valuable data read successfully.', status=STATUS_ENUM['OK'])
        except:
            self.main.print_error()
            raise

    #partially ported from 'readGazeContingency.R' script by ivan866, 23.11.2014
    def read_SMI_samples(self, settings_reader: object, multi_data: object) -> None:
        """Reads SMI samples from .txt files, after IDF Converter (iTools utility package) export.
        :param settings_reader: the object created at start of the program, and populated with parsed XML settings tree.
        :param multi_data: the main data structure to be populated with Pandas dataframes, one for each id/type combination.
        :return: None
        """
        for file_elem in settings_reader.gen_type_file('smi-samples'):
            file_path = settings_reader.get_path_attr_by_id('smi-samples', file_elem.get('id'), absolute=True)
            file_ext = os.path.splitext(file_path)[1]
            self.main.print_to_out('Reading samples ({0})...'.format(os.path.basename(file_path)))
            if file_ext.lower() == '.txt':
                self.main.print_to_out('Parsing {0} file.'.format(file_ext))
                skiprows = self._determine_skiprows(file_path, '##')
                metablock = []
                with open(file_path, encoding='UTF-8') as f:
                    for n in range(skiprows+1):
                        headers = f.readline()
                        metablock.append(headers)
                metablock = ''.join(metablock)
                #tested on SMI RED-m-HP data only
                avail_columns = [i for i in headers.strip().split('\t') if re.match('Time|Type|Trial|L Dia X \[px\]|L Dia Y \[px\]|L Mapped Diameter \[mm\]|L Raw X \[px\]|L Raw Y \[px\]|L POR X \[px\]|L POR Y \[px\]|R Dia X \[px\]|R Dia Y \[px\]|R Mapped Diameter \[mm\]|R Raw X \[px\]|R Raw Y \[px\]|R POR X \[px\]|R POR Y \[px\]|Timing|Latency|L Validity|R Validity|Pupil Confidence|L GVEC X|L GVEC Y|L GVEC Z|R GVEC X|R GVEC Y|R GVEC Z|Trigger', i)]
                multi_data.set_node('avail_columns', file_elem.get('id'), avail_columns)
                samples_data = pd.read_table(file_path,
                                             decimal=".", sep='\t', skiprows=skiprows, header=0, usecols=avail_columns, encoding='UTF-8',
                                             dtype={'Time':float, 'Type':str, 'Trial':int, 'Frame':str})
                samples_data['Time'] /= 1000000
                # оставляем достаточную точность, иначе ошибки округления
                samples_data = samples_data.round(6)
                #first line can be MSG type, but this is OK, we count from it anyway
                zero_time = samples_data.iloc[0]['Time']    #ABSOLUTE
                max_time = samples_data.iloc[-1]['Time'] - zero_time    #RELATIVE
                samples_data['Time'] -= zero_time
                samples_data = samples_data.loc[samples_data['Type']=='SMP']
                #4-ый столбец, как известно, кастуется в другой тип потому что там тип месседжа записан
                samples_data = samples_data.astype(dtype={samples_data.columns[3]: 'float64'}, copy=False)
                #MSG lines
                messages_data = pd.read_table(file_path,
                                              decimal=".", sep='\t', skiprows=skiprows, header=0, usecols=[0,1,2,3], encoding='UTF-8',
                                              dtype={'Time': float, 'Type': str, 'Trial': int})
                messages_data['Time'] /= 1000000
                messages_data = messages_data.round(6)
                messages_data['Time'] -= zero_time
                messages_data = messages_data.loc[messages_data['Type'] == 'MSG']
                messages_data.rename(columns={messages_data.columns[3]: "Text"}, inplace=True)
                messages_data['Text'] = messages_data['Text'].apply(lambda x: re.sub('# Message: (.*)', '\\1', str(x)))
                #MESSAGES block
                #messages are (conventionally) assumed to have duration
                #last message duration is assumed to be up to the end of the record
                #zero_time special interval added even if it itself equals 0
                #very first message
                message_tag = ET.Element('message')
                message_tag.set('time', '{:.6f}'.format(0))
                message_tag.set('duration', '{:.6f}'.format(messages_data.iloc[0]['Time']))
                message_tag.set('id', '_zeroTime')  # underscored special tag
                message_tag.set('text', '')
                # TODO should return list of messages and trials to settingsReader, and it parses / assigns them itself
                settings_reader.settings.append(message_tag)
                msg_num = 0
                for index, row in messages_data.reset_index().iterrows():   #we do not need ragged index
                    msg_num += 1
                    message_tag = ET.Element('message')
                    message_tag.set('time', '{:.6f}'.format(row['Time']))
                    if index < len(messages_data)-1:
                        duration = messages_data.iloc[index+1]['Time'] - row['Time']
                    else:
                        duration = max_time - row['Time']
                    message_tag.set('duration', '{:.6f}'.format(duration))
                    message_tag.set('id', str(msg_num))
                    #message_tag.set('trial', row['Trial'])
                    message_tag.set('text', row['Text'])
                    settings_reader.settings.append(message_tag)
                #TRIALS block
                #trials are assumed to have no gaps between them
                #FIXME trial start MSG text hard-coded
                try:
                    trial_data = pd.DataFrame([messages_data.iloc[index] for index,row in messages_data.reset_index().iterrows() if 'FixPoint' in row['Text'] or 'fixDot' in row['Text']])
                    trial_tag = ET.Element('trial')
                    trial_tag.set('start', '{:.6f}'.format(0))
                    trial_tag.set('duration', '{:.6f}'.format(trial_data.iloc[0]['Time']))
                    trial_tag.set('id', '_zeroTime')
                    trial_tag.set('text', '')
                    settings_reader.settings.append(trial_tag)
                    trial_num = 0
                    for index, row in trial_data.reset_index().iterrows():
                        trial_num += 1
                        trial_tag = ET.Element('trial')
                        trial_tag.set('start', '{:.6f}'.format(row['Time']))
                        if index < len(trial_data)-1:
                            duration = trial_data.iloc[index+1]['Time'] - row['Time']
                        else:
                            duration = max_time - row['Time']
                        trial_tag.set('duration', '{:.6f}'.format(duration))
                        trial_tag.set('id', str(trial_num))
                        trial_tag.set('text', row['Text'])
                        settings_reader.settings.append(trial_tag)
                    self.main.print_to_out('Messages and trial intervals auto-parsed, copy of settings will be updated.')
                    #TODO 'tagged': True
                except IndexError:
                    pass
                #TRIGGERS block (applicable for SMI HiSpeed)
                #TODO not implemented
                #get metadata
                #values assumed to be integers
                sample_rate = int(re.search('Sample Rate:\t(\d+)', metablock).groups()[0])
                screen_size_px = re.search('Calibration Area:\t(\d+)\t(\d+)', metablock).groups()
                screen_width_px, screen_height_px = int(screen_size_px[0]), int(screen_size_px[1])
                screen_size_mm = re.search('Stimulus Dimension \[mm\]:\t(\d+)\t(\d+)', metablock).groups()
                screen_width_mm, screen_height_mm = int(screen_size_mm[0]), int(screen_size_mm[1])
                head_distance_mm = int(re.search('Head Distance \[mm\]:\t(\d+)', metablock).groups()[0])
                #degrees, assuming the eyesight axis is centered around the screen (spherical eye model)
                screen_width_deg = Utils.get_separation(-screen_width_mm / 2, 0, screen_width_mm / 2, 0, z=head_distance_mm, mode=COORDINATES_MODE['FROM_CARTESIAN'])
                screen_height_deg = Utils.get_separation(-screen_height_mm / 2, 0, screen_height_mm / 2, 0, z=head_distance_mm, mode=COORDINATES_MODE['FROM_CARTESIAN'])
                screen_Hres_mm, screen_Vres_mm = screen_width_mm / screen_width_px, screen_height_mm / screen_height_px
                metadata = {'sample_rate': sample_rate,
                            'screen_width_px': screen_width_px, 'screen_height_px': screen_height_px,
                            'screen_width_mm': screen_width_mm, 'screen_height_mm': screen_height_mm,
                            'screen_width_deg': screen_width_deg, 'screen_height_deg': screen_height_deg,
                            'screen_Hres_mm': screen_Hres_mm, 'screen_Vres_mm': screen_Vres_mm,
                            'head_distance_mm': head_distance_mm}
                #experiment (record) metadata is in special property, not a separate data channel
                samples_data.metadata = metadata
            elif file_ext.lower() == '.idf':
                self.main.print_to_out('ERROR: Cannot parse .idf files. Convert them to .txt first.')
                raise NotImplementedError
            elif file_ext.lower() == '.csv':
                self.main.print_to_out('Parsing {0} file.'.format(file_ext))
                samples_data = pd.read_csv(file_path, sep='\t')
            else:
                self.main.print_to_out('Unknown file format.')
            multi_data.set_node('smi-samples', file_elem.get('id'), samples_data)
            multi_data.set_node('smi-messages', file_elem.get('id'), messages_data)

    #UTILS methods
    def _determine_skiprows(self, file:str, comment_str:str) -> int:
        """
        :param file:
        :param comment_str:
        :return:
        """
        with open(file, encoding='UTF-8') as f:
            lineNum = 0
            line = f.readline()
            while line.startswith(comment_str):
                lineNum += 1
                line = f.readline()
            return lineNum