#!/usr/bin/env python3
# -*- coding: utf8 -*-
from itertools import compress
from datetime import timedelta
import numpy as np
from scipy.signal import convolve, cspline1d, savgol_filter
from pandas import DataFrame
from parsers.DataReader import SMI_SAMPLE_HEADERS
from utils import Utils
from utils.Utils import COORDINATES_MODE, STATUS_ENUM

FILTERS_SMOOTHING = {'SAVGOL': 'savgol',
                     '1DSPLINE': '1dspline',
                     '2DSPLINE': '2dspline',    #TODO NotImplemented
                     'CONV': 'conv'}

#TODO must be refactored to multidata_onechannel and multidata_full hierarchy
class MultiData():
    """Basic data structure to hold various data channels.
    Includes some helper methods to select different data channels and filter by timestamp.
    """
    def __init__(self, main):
        self.main = main
        self.settings_reader = main.settings_reader
        self.multi_data = {}
        self.multi_data['avail_columns'] = {}
        self.multi_data['smi-samples'] = {}
        self.multi_data['smi-messages'] = {}
        self.multi_data['ibdt-events'] = {}
        self.multi_data['ibdt-events-filtered'] = {}
        self.multi_data['ibdt-events-bad'] = {}
        self._attributes = {}
        self.empty = True

    def gen_channel_ids(self, channel:str) -> tuple:
        """Generator of ids present in multiData in particular channel (i.e. 'type' tag).
        :param channel:
        :return: Tuple with current channel and id, if such id present in multiData.
        """
        if self.settings_reader._check() and self._check():
            channel_zero_name = self.settings_reader._subst_versatile_channels(channel)
            type_list = self.settings_reader.get_types(channel_zero_name)
            #на случай если нет комбинированного типа в настройках
            if not len(type_list):
                type_list = self.settings_reader.get_types(channel)
            for file in type_list:
                id = file.get('id')
                if self._has_channel_by_id(channel, id):
                    yield (channel, id)

    def reset(self) -> None:
        """Makes this multiData empty.
        :return: None.
        """
        self.__init__(self.main)

    def set_node(self, channel: str, id: str, data: object) -> None:
        """Sets chosen node in hierarchy of multiData to given data object.
        :param channel: string of type from settings.
        :param id: string of channel id from settings.
        :param data: object with some data, probably pandas dataframe or python list.
        :return:
        """
        #self.main.logger.debug('setting data node...')
        self.multi_data[channel][id] = data
        self.empty = False

    #FILTERING methods
    def get_channel_by_id(self, channel:str, id:str, format:str= 'as_is') -> object:
        """Returns what's inside multiData[channel][id] dict hierarchy, possibly converting to dataframe.
        :param channel: string of type from settings.
        :param id: string of channel id from settings.
        :param format: specify this str to convert to DataFrame type
        :return: data object, can be converted to dataframe.
        """
        #self.main.logger.debug('get channel by id')
        result = self.multi_data[channel][id]
        if format=='dataframe':
            if type(result) == DataFrame:
                return result
            #elif type(result) == your custom data type:
            #    return yourParserFunction(self.main, result, settingsReader = self.settingsReader)
            else:
                self.main.print_to_out('WARNING: Converting this type of data to DataFrame not implemented.')
                return None
        elif format=='as_is':
            return result

    def get_channel_and_tag(self, channel:str, id:str, block:str, format:str= 'as_is', ignore_empty:bool=True) -> object:
        """Returns what's inside the given channel, but tags the data by record tag, id and interval first.
        :param channel: 
        :param id:
        :param block: which block to tag by.
        :param format: str for type conversion
        :param ignore_empty: Whether to cut off the empty and utility intervals.
        :return: 
        """
        ch_data = self.get_channel_by_id(channel, id, format=format)
        channel_zero_name = self.settings_reader._subst_versatile_channels(channel)
        #zeroTime tag only applicable to INTERVALS block, because it is predefined for data channels
        if block == 'interval':
            start_from = self.settings_reader.get_zero_time_by_id(channel_zero_name, id)
        else:
            start_from = 0
        path_attr = self.settings_reader.get_path_attr_by_id(type=channel_zero_name, id=id)
        if ('Record tag' not in ch_data.columns) and ('Id' not in ch_data.columns):
            ch_data.insert(2, 'Record tag', path_attr)
            ch_data.insert(3, 'Id', id)
        #FIXME hotfix
        #elif 'Id 2' not in ch_data.columns:
        #    ch_data['Record tag'] = path_attr
        #    ch_data.insert(8, 'Id 2', id)
        return self.tag_intervals(ch_data, start_from, block=block, ignore_empty=ignore_empty)

    def get_data_between(self, data:object, time_start:object, time_end:object) -> object:
        """Selects and returns those data where timestamp is in given interval range.
        Assuming timestamp in column 0.
        :param data: data to trim from, usually after getChannelById method.
        :param time_start: timestamp to begin data with in 'M:S.f' str or timedelta format.
        :param time_end: timestamp to end data with in 'M:S.f' str or timedelta format.
        :return: Trimmed data.
        """
        time_index = Utils.locate_specific_column(data.columns)
        parsed_time = Utils.parse_time_v(data.iloc[:, time_index])
        try:
            data.insert(1, 'Timedelta', parsed_time)
        except ValueError:
            pass
        if type(time_start) is not timedelta:
            time_start = Utils.parse_time(time_start)
        if type(time_end) is not timedelta:
            time_end = Utils.parse_time(time_end)
        return data.loc[(data['Timedelta'] >= time_start) & (data['Timedelta'] < time_end)]

    def get_data_interval(self, data:object, start_from:object, interval:str, block:str) -> object:
        """Selects and returns data where timestamp is inside interval defined by its id name.
        :param data: data to trim from, usually after getChannelById method.
        :param start_from: Time value to start first interval from.
        :param interval: id of interval in str format from settings.
        :param block:
        :return: Trimmed data.
        """
        if type(start_from) is not timedelta:
            start_from = Utils.parse_time(start_from)
        start_time= self.settings_reader.get_start_time_by_id(interval, block=block) + start_from
        end_time = self.settings_reader.get_end_time_by_id(interval, block=block) + start_from
        return self.get_data_between(data, start_time, end_time)

    def tag_intervals(self, ch_data:object, start_from:object, block:str, ignore_empty:bool=True) -> DataFrame:
        """Tags given data by intervals, then returns a single dataframe.
        :param ch_data: data to stack intervals from, usually after getChannelById method.
        :param start_from: zero_time to start from.
        :param block: which type of block to tag by.
        :param ignore_empty: Whether to cut off the empty and utility intervals.
        :return: DataFrame object ready to group by intervals.
        """
        data = []
        ints = self.settings_reader.get_intervals(block=block, ignoreEmpty=ignore_empty)
        #NOTE 'id' hard-coded
        for interval in ints:
            int_data = self.get_data_interval(ch_data, start_from, interval.get('id'), block=block)
            int_data.insert(4, block, interval.get('id'))
            #int_data.insert(5, '{0} duration'.format(block), interval.get('duration'))
            data.append(int_data)
        #case when there is no interval block in settings at all - nothing to tag
        if len(ints) == 0:
            return ch_data
        if len(ints) == 1:
            data = data[0]
        else:
            data = data[0].append(data[1:])
        zero_based = []
        zero_time = data.iloc[0, 0]
        for timestamp in data.iloc[:, 0]:
            zero_based.append(timestamp - zero_time)
        #FIXME should be no duplicates
        data.insert(1, 'TimestampZeroBased', zero_based, allow_duplicates=True)
        #inheriting metadata
        try:
            data.metadata = ch_data.metadata
        except AttributeError:
            pass
        return data

    #EYE MOVEMENT methods
    #TODO #FIXME на RED-m расстояние до экрана меняется, так что скорость надо вычислять по GVECX/Y/Z; здесь она вычисляется неточно
    #TODO profile
    def get_velocity(self, samples_data:DataFrame, id:str, smooth:str, convert_to_deg:bool) -> DataFrame:
        """Method for calculating eye velocity, normally pixels converted to degrees first.
        :param samples_data: dataframe to operate on, containing appropriate eyetracker columns (Time, X, Y, etc.).
        :param id: file id tag from settings
        :param smooth: algo to use, normally passed by command line argument.
        :param convert_to_deg: whether data is passed in raw pixel values or visual angle degrees.
        :return: data with added *Velocity columns (and smoothed position columns).
        """
        #FIXME data column names hard-coded, need refactor to global name dictionary mapper (SMI, -[Tobii] variants)
        #  mapping goes to multi_data metadata property
        #TODO B side (binocular) variant not implemented (applicable for SMI ETG)
        samples_data.metadata['equivalent'] = False
        #NOTE POR is equivalent, but Dia, GVEC ... are not!
        try:
            if all(samples_data['L POR X [px]'] == samples_data['R POR X [px]']) and all(samples_data['L POR Y [px]'] == samples_data['R POR Y [px]']):
                self.main.print_to_out('Left and right POR fields detected equivalent. Working with one channel only.')
                samples_data.metadata['equivalent'] = True
        except KeyError:
            pass
        metadata = samples_data.metadata
        self.main.print_to_out('WARNING: Dimensions metadata from samples file is considered correct and precise, and used in pixel-to-degree conversions.')
        self.main.print_to_out('WARNING: GVEC field is not used for angle calculations. *Deg fields and Velocity being imprecise!')
        self.main.print_to_out('Now calculating velocity, be patient.')
        sides = ['L', 'R']
        if metadata['equivalent']:
            #должен быть ведущий глаз
            sides = ['R']
        else:
            sides = list(compress(sides, [self._has_column('L POR X [px]', id), self._has_column('R POR X [px]', id)]))
        # DISTANCE
        x = samples_data[SMI_SAMPLE_HEADERS['X']]
        y = samples_data[SMI_SAMPLE_HEADERS['Y']]
        distance = np.hstack((0, np.hypot(x.iloc[1:].reset_index(drop=True) - x.iloc[0:(len(x) - 1)].reset_index(drop=True),
                                          y.iloc[1:].reset_index(drop=True) - y.iloc[0:(len(y) - 1)].reset_index(drop=True))))
        timelag = np.hstack((1, np.diff(samples_data[SMI_SAMPLE_HEADERS['TIMESTAMP']])))
        samples_data['distance_px'] = distance
        samples_data['velocity_pxms'] = (distance / timelag) / 1000
        #TODO skipping one channel if same
        for side in sides:
            for dim in ['X', 'Y']:
                # smoothing
                data_to_smooth = samples_data['{0} POR {1} [px]'.format(side, dim)]
                if smooth == FILTERS_SMOOTHING['SAVGOL']:
                    samples_data['{0}POR{1}PxSmoothed'.format(side, dim)] = savgol_filter(data_to_smooth, 15, 2)
                elif smooth == FILTERS_SMOOTHING['1DSPLINE']:
                    #scipy.interpolate.UnivariateSpline(x,y, k=1).get_coeffs()
                    #TODO lamb param should probably be adaptive to data frequency
                    samples_data['{0}POR{1}PxSmoothed'.format(side, dim)] = cspline1d(np.array(data_to_smooth), lamb=3)
                elif smooth == FILTERS_SMOOTHING['2DSPLINE']:
                    raise NotImplementedError
                elif smooth == FILTERS_SMOOTHING['CONV']:
                    #width and shape of convolution, equivalent to moving average if all 1
                    win = np.array([1,1,1,1,1,1])
                    samples_data['{0}POR{1}PxSmoothed'.format(side, dim)] = convolve(np.array(data_to_smooth), in2=win, mode='same') / win.sum()
                else:
                    self.main.print_to_out('ERROR: Invalid smoothing function specified.')
                samples_data.metadata['velocity_smooth'] = smooth
                if dim == 'X':
                    screen_dim = metadata['screen_width_px']
                    screen_res = metadata['screen_Hres_mm']
                    multiplier = 1
                elif dim == 'Y':
                    screen_dim = metadata['screen_height_px']
                    screen_res = metadata['screen_Vres_mm']
                    multiplier = -1
                if not convert_to_deg:
                    self.main.print_to_out('ERROR: Raw pixels in data are currently assumed, column names hard-coded.')
                    raise NotImplementedError
                else:
                    #converting to DEGREES
                    samples_data['{0}POR{1}Mm'.format(side, dim)]  = multiplier * (samples_data['{0} POR {1} [px]'.format(side, dim)] - screen_dim / 2) * screen_res
                    coords_mm = samples_data['{0}POR{1}Mm'.format(side, dim)]
                    samples_data['{0}POR{1}Deg'.format(side, dim)] = np.sign(coords_mm) * coords_mm.apply(lambda x: Utils.get_separation(x, 0, 0, 0, z=metadata['head_distance_mm'], mode=COORDINATES_MODE['FROM_CARTESIAN']))
                    #----
                    samples_data['{0}POR{1}MmSmoothed'.format(side, dim)] = multiplier * (samples_data['{0}POR{1}PxSmoothed'.format(side, dim)] - screen_dim / 2) * screen_res
                    coords_mm = samples_data['{0}POR{1}MmSmoothed'.format(side, dim)]
                    samples_data['{0}POR{1}DegSmoothed'.format(side, dim)] = np.sign(coords_mm) * coords_mm.apply(lambda x: Utils.get_separation(x, 0, 0, 0, z=metadata['head_distance_mm'], mode=COORDINATES_MODE['FROM_CARTESIAN']))
            #VELOCITY calculation
            x = samples_data['{0}PORXDeg'.format(side)]
            y = samples_data['{0}PORYDeg'.format(side)]
            #NOTE [1] != [1:].iloc[0]
            row = DataFrame({'x1':x[1:].reset_index(drop=True),
                             'y1':y[1:].reset_index(drop=True),
                             'x0':x[:(len(x) - 1)].reset_index(drop=True),
                             'y0':y[:(len(y) - 1)].reset_index(drop=True)})
            seps = row.apply(lambda rowApply: Utils.get_separation(x1=rowApply['x1'],
                                                                   y1=rowApply['y1'],
                                                                   x2=rowApply['x0'],
                                                                   y2=rowApply['y0'],
                                                                   z=metadata['head_distance_mm'], mode=COORDINATES_MODE['FROM_POLAR']), axis=1)
            separation = np.hstack((1, seps))
            samples_data['{0}Velocity'.format(side)] = separation / timelag
            #----
            x = samples_data['{0}PORXDegSmoothed'.format(side)]
            y = samples_data['{0}PORYDegSmoothed'.format(side)]
            row = DataFrame({'x1': x[1:].reset_index(drop=True),
                             'y1': y[1:].reset_index(drop=True),
                             'x0': x[:(len(x) - 1)].reset_index(drop=True),
                             'y0': y[:(len(y) - 1)].reset_index(drop=True)})
            seps = row.apply(lambda row_apply: Utils.get_separation(x1=row_apply['x1'],
                                                                    y1=row_apply['y1'],
                                                                    x2=row_apply['x0'],
                                                                    y2=row_apply['y0'],
                                                                    z=metadata['head_distance_mm'], mode=COORDINATES_MODE['FROM_POLAR']), axis=1)
            separation = np.hstack((1, seps))
            samples_data['{0}VelocitySmoothed'.format(side)] = separation / timelag
        self.main.print_to_out('Done.', status=STATUS_ENUM['OK'])
        return samples_data

    #SANITY check methods
    def _has_column(self, column:str, id:str) -> bool:
        """Checks if multiData contains such column in its gaze channel.
        :param column: Column name from Tobii gaze data.
        :param id: string of channel id from settings.
        :return: True if column present, False otherwise.
        """
        return column in self.multi_data['avail_columns'][id]

    def _has_all_columns(self, columns:list, id:str) -> bool:
        """Checks if multiData contains ALL these columns passed in list.
        :param columns: List of strings with column names.
        :param id: string of channel id from settings.
        :return: True if all columns present, False otherwise.
        """
        for col in columns:
            if col not in self.multi_data['avail_columns'][id]:
                return False
        return True

    def _has_channel_by_id(self, channel:str, id:str) -> bool:
        """Checks if multiData contains this channel.id node in its hierarchy.
        :param channel: string of type from settings.
        :param id: string of channel id from settings.
        :return: True if such id in such channel present, False otherwise.
        """
        try:
            self.multi_data[channel][id]
            return True
        except KeyError:
            return False

    def _check(self) -> bool:
        """Helper method that checks if multiData present at all.
        :return: True if it is, False otherwise.
        """
        #self.main.logger.debug('check data')
        if not self.empty:
            return True
        else:
            self.main.print_to_out('WARNING: No data loaded yet. Read data first!')
            return False